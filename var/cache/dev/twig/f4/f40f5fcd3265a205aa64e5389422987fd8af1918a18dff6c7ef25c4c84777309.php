<?php

/* frontal/nosotros.html.twig */
class __TwigTemplate_0ab290059b35672f87fef243b1fd191dae88b957075557b8936fe3950de00f47 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/nosotros.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/nosotros.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/nosotros.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo "Sobre Nosotros ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 7
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Sobre Nosotros</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Nosotros</li>
        </ol>
        <!-- Image Header -->
        <img class=\"img-fluid rounded mb-4\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/nosotros.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <h2>Nuestro equipo</h2>
        <!-- Marketing Icons Section -->
        <div class=\"row\">
            <div class=\"col-lg-4 mb-4\">
                <div class=\"card h-100\">
                    ";
        // line 29
        echo "                    <img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/gerencia.jpg"), "html", null, true);
        echo "\" width=\"348\" alt=\"gerencia\">
                    <div class=\"card-body\">
                        <h4 class=\"card-title text-center\">Gerencia</h4>
                        <h6 class=\"card-subtitle mb-2 text-muted text-center\">Los nietos del fundador</h6>
                        <p class=\"card-text text-justify\">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Sapiente esse necessitatibus neque.</p>
                    </div>
                    <div class=\"card-footer\">
                        <a href=\"";
        // line 37
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nosotros", array("ruta" => "gerencia"));
        echo "\" class=\"btn btn-primary\">Leer más</a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4 mb-4\">
                <div class=\"card h-100\">
                    ";
        // line 44
        echo "                    <img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/historia.jpg"), "html", null, true);
        echo "\" width=\"348\" alt=\"historia\">
                    <div class=\"card-body\">
                        <h4 class=\"card-title text-center\">Historia</h4>
                        <h6 class=\"card-subtitle mb-2 text-muted text-center\">Orígenes</h6>
                        <p class=\"card-text text-justify\">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Reiciendis ipsam eos, nam perspiciatis natus commodi similique totam consectetur praesentium
                            molestiae atque exercitationem ut consequuntur, sed eveniet, magni nostrum sint fuga.</p>
                    </div>
                    <div class=\"card-footer\">
                        <a href=\"";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nosotros", array("ruta" => "historia"));
        echo "\" class=\"btn btn-primary\">Leer más</a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4 mb-4\">
                <div class=\"card h-100\">
                    ";
        // line 60
        echo "                    <img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/equipo.jpg"), "html", null, true);
        echo "\" width=\"348\" alt=\"equipo\">
                    <div class=\"card-body\">
                        <h4 class=\"card-title text-center\">Equipo</h4>
                        <h6 class=\"card-subtitle mb-2 text-muted text-center\">Compras y Distribución</h6>
                        <p class=\"card-text text-justify\">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Sapiente esse necessitatibus neque.</p>
                    </div>
                    <div class=\"card-footer\">
                        <a href=\"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nosotros", array("ruta" => "equipo"));
        echo "\" class=\"btn btn-primary\">Leer más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/nosotros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 68,  144 => 60,  135 => 53,  122 => 44,  113 => 37,  101 => 29,  92 => 22,  84 => 17,  72 => 7,  63 => 6,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{% block titulo %}Sobre Nosotros {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Sobre Nosotros</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('homepage') }}\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Nosotros</li>
        </ol>
        <!-- Image Header -->
        <img class=\"img-fluid rounded mb-4\" src=\"{{ asset('img/nosotros.jpg') }}\" alt=\"\">
        <h2>Nuestro equipo</h2>
        <!-- Marketing Icons Section -->
        <div class=\"row\">
            <div class=\"col-lg-4 mb-4\">
                <div class=\"card h-100\">
                    {#<h4 class=\"card-header\">Equipo</h4>#}
                    <img src=\"{{ asset('img/gerencia.jpg') }}\" width=\"348\" alt=\"gerencia\">
                    <div class=\"card-body\">
                        <h4 class=\"card-title text-center\">Gerencia</h4>
                        <h6 class=\"card-subtitle mb-2 text-muted text-center\">Los nietos del fundador</h6>
                        <p class=\"card-text text-justify\">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Sapiente esse necessitatibus neque.</p>
                    </div>
                    <div class=\"card-footer\">
                        <a href=\"{{ path('nosotros',{'ruta':'gerencia'}) }}\" class=\"btn btn-primary\">Leer más</a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4 mb-4\">
                <div class=\"card h-100\">
                    {#<h4 class=\"card-header\">Card Title</h4>#}
                    <img src=\"{{ asset('img/historia.jpg') }}\" width=\"348\" alt=\"historia\">
                    <div class=\"card-body\">
                        <h4 class=\"card-title text-center\">Historia</h4>
                        <h6 class=\"card-subtitle mb-2 text-muted text-center\">Orígenes</h6>
                        <p class=\"card-text text-justify\">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Reiciendis ipsam eos, nam perspiciatis natus commodi similique totam consectetur praesentium
                            molestiae atque exercitationem ut consequuntur, sed eveniet, magni nostrum sint fuga.</p>
                    </div>
                    <div class=\"card-footer\">
                        <a href=\"{{ path('nosotros',{'ruta':'historia'}) }}\" class=\"btn btn-primary\">Leer más</a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4 mb-4\">
                <div class=\"card h-100\">
                    {#<h4 class=\"card-header\">Card Title</h4>#}
                    <img src=\"{{ asset('img/equipo.jpg') }}\" width=\"348\" alt=\"equipo\">
                    <div class=\"card-body\">
                        <h4 class=\"card-title text-center\">Equipo</h4>
                        <h6 class=\"card-subtitle mb-2 text-muted text-center\">Compras y Distribución</h6>
                        <p class=\"card-text text-justify\">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Sapiente esse necessitatibus neque.</p>
                    </div>
                    <div class=\"card-footer\">
                        <a href=\"{{ path('nosotros',{'ruta':'equipo'}) }}\" class=\"btn btn-primary\">Leer más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}", "frontal/nosotros.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\frontal\\nosotros.html.twig");
    }
}
