<?php

/* frontal/locales.html.twig */
class __TwigTemplate_5cd0fe00f46dc5e45737d0e79ac454541b73d12590102e11535e3d0942e8860e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/locales.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/locales.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/locales.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo "Locales ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 7
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Locales</small>
        </h1>
        ";
        // line 15
        echo "        ";
        // line 16
        echo "        ";
        if ((((isset($context["sitio"]) || array_key_exists("sitio", $context) ? $context["sitio"] : (function () { throw new Twig_Error_Runtime('Variable "sitio" does not exist.', 16, $this->source); })()) == "central") || ((isset($context["sitio"]) || array_key_exists("sitio", $context) ? $context["sitio"] : (function () { throw new Twig_Error_Runtime('Variable "sitio" does not exist.', 16, $this->source); })()) == "todos"))) {
            // line 17
            echo "            <!-- Local Agrela -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3073.44401512575!2d-8.4219075788343!3d43.35726570275931!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2e7cf29c5a1801%3A0x187698011fa57edf!2sCalle+Pasteur%2C+14%2C+15008+La+Coru%C3%B1a!5e0!3m2!1ses!2ses!4v1541544687410\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Cafés Mauro
                        <br>Pol.Agrela, Pasteur, 14-15008 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 981274012
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        ";
        }
        // line 48
        echo "
        ";
        // line 50
        echo "        ";
        if ((((isset($context["sitio"]) || array_key_exists("sitio", $context) ? $context["sitio"] : (function () { throw new Twig_Error_Runtime('Variable "sitio" does not exist.', 50, $this->source); })()) == "ruaNova") || ((isset($context["sitio"]) || array_key_exists("sitio", $context) ? $context["sitio"] : (function () { throw new Twig_Error_Runtime('Variable "sitio" does not exist.', 50, $this->source); })()) == "todos"))) {
            // line 51
            echo "            <!-- Local Rua Nova -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4632.25942513763!2d-8.40440362285743!3d43.37081748708746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7a35331274711cf0!2sCaf%C3%A9+Siboney!5e1!3m2!1ses!2ses!4v1544095477090\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Café & Boutique Mauro
                        <br> Rua Nova, 28 - 15003 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 981 227 836
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        ";
        }
        // line 82
        echo "
        ";
        // line 84
        echo "        ";
        if ((((isset($context["sitio"]) || array_key_exists("sitio", $context) ? $context["sitio"] : (function () { throw new Twig_Error_Runtime('Variable "sitio" does not exist.', 84, $this->source); })()) == "ruaFerrol") || ((isset($context["sitio"]) || array_key_exists("sitio", $context) ? $context["sitio"] : (function () { throw new Twig_Error_Runtime('Variable "sitio" does not exist.', 84, $this->source); })()) == "todos"))) {
            // line 85
            echo "            <!-- Local Rúa Ferrol -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2900.5814027398446!2d-8.410197908706213!3d43.364865892505705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xebf814e6bdd12fc0!2sCaf%C3%A9+Siboney!5e0!3m2!1ses!2ses!4v1544095393695\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>

                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Café & Boutique Mauro
                        <br> Rua Ferrol, 14 - 15004 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 881 899 142
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        ";
        }
        // line 117
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/locales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 117,  164 => 85,  161 => 84,  158 => 82,  125 => 51,  122 => 50,  119 => 48,  86 => 17,  83 => 16,  81 => 15,  72 => 7,  63 => 6,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{% block titulo %}Locales {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Locales</small>
        </h1>
        {#{{ dump(sitio) }}#}
        {#********************************************************************************#}
        {% if sitio=='central' or sitio=='todos' %}
            <!-- Local Agrela -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3073.44401512575!2d-8.4219075788343!3d43.35726570275931!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2e7cf29c5a1801%3A0x187698011fa57edf!2sCalle+Pasteur%2C+14%2C+15008+La+Coru%C3%B1a!5e0!3m2!1ses!2ses!4v1541544687410\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Cafés Mauro
                        <br>Pol.Agrela, Pasteur, 14-15008 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 981274012
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        {% endif %}

        {#********************************************************************************#}
        {% if sitio=='ruaNova' or sitio=='todos' %}
            <!-- Local Rua Nova -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4632.25942513763!2d-8.40440362285743!3d43.37081748708746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7a35331274711cf0!2sCaf%C3%A9+Siboney!5e1!3m2!1ses!2ses!4v1544095477090\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Café & Boutique Mauro
                        <br> Rua Nova, 28 - 15003 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 981 227 836
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        {% endif %}

        {#********************************************************************************#}
        {% if sitio=='ruaFerrol' or sitio=='todos' %}
            <!-- Local Rúa Ferrol -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2900.5814027398446!2d-8.410197908706213!3d43.364865892505705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xebf814e6bdd12fc0!2sCaf%C3%A9+Siboney!5e0!3m2!1ses!2ses!4v1544095393695\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>

                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Café & Boutique Mauro
                        <br> Rua Ferrol, 14 - 15004 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 881 899 142
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        {% endif %}
    </div>
{% endblock %}", "frontal/locales.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\frontal\\locales.html.twig");
    }
}
