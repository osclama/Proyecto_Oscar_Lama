<?php

/* frontal/registro.html.twig */
class __TwigTemplate_8099ee0f396db34d2facfba91aa1176e5c83461bb80ef836352a7b730ba184cd extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/registro.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/registro.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/registro.html.twig"));

        // line 4
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["formularioUsuario"]) || array_key_exists("formularioUsuario", $context) ? $context["formularioUsuario"] : (function () { throw new Twig_Error_Runtime('Variable "formularioUsuario" does not exist.', 4, $this->source); })()), array(0 => "form/cafeForm.html.twig"), true);
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo "Registro Usuario ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 10
        echo "    ";
        // line 11
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\"> Cafés Mauro
            <small>Registro de usuario</small>
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Registro</li>
        </ol>
        <!-- Intro Content -->

        <div class=\"row\">
            <div class=\"col-lg-6\">
                ";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formularioUsuario"]) || array_key_exists("formularioUsuario", $context) ? $context["formularioUsuario"] : (function () { throw new Twig_Error_Runtime('Variable "formularioUsuario" does not exist.', 28, $this->source); })()), 'form_start');
        echo "
                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["formularioUsuario"]) || array_key_exists("formularioUsuario", $context) ? $context["formularioUsuario"] : (function () { throw new Twig_Error_Runtime('Variable "formularioUsuario" does not exist.', 29, $this->source); })()), 'widget');
        echo "
                ";
        // line 30
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formularioUsuario"]) || array_key_exists("formularioUsuario", $context) ? $context["formularioUsuario"] : (function () { throw new Twig_Error_Runtime('Variable "formularioUsuario" does not exist.', 30, $this->source); })()), 'form_end');
        echo "
            </div>
            <div class=\"col-lg-6\">
                <h3>¿Ya estás registrado?</h3>
                <p>Si ya tienes una cuenta de usuario y quieres volver a la página previa no tienes más que pulsar en <a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logado");
        echo "\"><strong>LOGIN</strong></a></p>
                <h4>Advertencia Importante</h4>
                <p>Asegúrese de registrar un tlf de contacto válido, en caso de no poder contactar con usted para confirmar la reserva nos reservamos el derecho de cancelar esta.</p>
            </div>
        </div>

    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/registro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 34,  107 => 30,  103 => 29,  99 => 28,  88 => 20,  77 => 11,  75 => 10,  66 => 9,  48 => 6,  38 => 2,  36 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{#Se usa el mismo template que definimos para los nuevos cafés#}
{% form_theme formularioUsuario 'form/cafeForm.html.twig' %}

{% block titulo %}Registro Usuario {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    {#Sacado de la página about de la plantilla#}
    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\"> Cafés Mauro
            <small>Registro de usuario</small>
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('homepage') }}\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Registro</li>
        </ol>
        <!-- Intro Content -->

        <div class=\"row\">
            <div class=\"col-lg-6\">
                {{ form_start(formularioUsuario) }}
                {{ form_widget(formularioUsuario) }}
                {{ form_end(formularioUsuario) }}
            </div>
            <div class=\"col-lg-6\">
                <h3>¿Ya estás registrado?</h3>
                <p>Si ya tienes una cuenta de usuario y quieres volver a la página previa no tienes más que pulsar en <a href=\"{{ path('logado') }}\"><strong>LOGIN</strong></a></p>
                <h4>Advertencia Importante</h4>
                <p>Asegúrese de registrar un tlf de contacto válido, en caso de no poder contactar con usted para confirmar la reserva nos reservamos el derecho de cancelar esta.</p>
            </div>
        </div>

    </div>
{% endblock %}", "frontal/registro.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\frontal\\registro.html.twig");
    }
}
