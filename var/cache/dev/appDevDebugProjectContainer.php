<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerWal9cil\appDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerWal9cil/appDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerWal9cil.legacy');

    return;
}

if (!\class_exists(appDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerWal9cil\appDevDebugProjectContainer::class, appDevDebugProjectContainer::class, false);
}

return new \ContainerWal9cil\appDevDebugProjectContainer(array(
    'container.build_hash' => 'Wal9cil',
    'container.build_id' => '5457f815',
    'container.build_time' => 1544195643,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerWal9cil');
