<?php

/* base.html.twig */
class __TwigTemplate_eaa4762b758c2a907c124f2153a76764e90007dbff4e66819afc00339deb2a47 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\"/>
    <title>";
        // line 5
        $this->displayBlock('titulo', $context, $blocks);
        echo "</title>
    ";
        // line 7
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    ";
        // line 9
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/modern-business.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/estilos.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 13
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\"/>
    ";
        // line 15
        echo "    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.5.0/css/all.css\"
          integrity=\"sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU\" crossorigin=\"anonymous\">


</head>
<body>
<!-- Menú de navegación -->
<nav class=\"navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top\">
    <div class=\"container\">
        <a class=\"navbar-brand font-italic \" href=\"";
        // line 24
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\"><h4>Cafés Mauro</h4></a>
        <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\"
                data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\"
                aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nosotros");
        echo "\">Nosotros</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias");
        echo "\">Cafés</a>
                </li>
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownBlog\" data-toggle=\"dropdown\"
                       aria-haspopup=\"true\" aria-expanded=\"false\">
                        Contactar
                    </a>
                    <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"navbarDropdownBlog\">
                        <a class=\"dropdown-item\" href=\"";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contactar", array("sitio" => "central"));
        echo "\">Central</a>
                        <a class=\"dropdown-item\" href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contactar", array("sitio" => "ruaNova"));
        echo "\">Rúa Nova</a>
                        <a class=\"dropdown-item\" href=\"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contactar", array("sitio" => "ruaFerrol"));
        echo "\">Rúa Ferrol</a>
                    </div>
                </li>
                ";
        // line 51
        echo "                ";
        if (twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", array())) {
            // line 52
            echo "                    ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 53
                echo "<li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownBlog\" data-toggle=\"dropdown\"
                               aria-haspopup=\"true\" aria-expanded=\"false\">Gestión</a>
                            <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"navbarDropdownBlog\">
                                <a class=\"dropdown-item\" href=\"";
                // line 57
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevaCategoria");
                echo "\">Nueva Categoría</a>
                                <a class=\"dropdown-item\" href=\"";
                // line 58
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edicionCategorias");
                echo "\">Editar Categorías</a>

                                <a class=\"dropdown-item\" href=\"";
                // line 60
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevoCafe");
                echo "\">Nuevo Café</a>
                                <a class=\"dropdown-item\" href=\"";
                // line 61
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edicionCafes");
                echo "\">Editar Cafés</a>
                                <a class=\"dropdown-item\" href=\"";
                // line 62
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edicionReservas");
                echo "\">Editar Reservas</a>
                                <a class=\"dropdown-item\" href=\"";
                // line 63
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edicionUsuarios");
                echo "\">Editar Usuarios</a>

                            </div>
                        </li>
                    ";
            } else {
                // line 68
                echo "                        <li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownBlog\" data-toggle=\"dropdown\"
                               aria-haspopup=\"true\" aria-expanded=\"false\">Reservas</a>
                            <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"navbarDropdownBlog\">
                                <a class=\"dropdown-item\" href=\"";
                // line 72
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevaReserva");
                echo "\">Nueva</a>
                                <a class=\"dropdown-item\" href=\"";
                // line 73
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("reservas");
                echo "\">Lista</a>
                            </div>
                        </li>
                    ";
            }
            // line 77
            echo "                    <li class=\"nav-item\">
                        ";
            // line 79
            echo "                        <a class=\"nav-link\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\">Salir</a>
                    </li>
                ";
        } else {
            // line 82
            echo "                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"";
            // line 83
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logado");
            echo "\">Usuarios</a>
                    </li>
                ";
        }
        // line 86
        echo "            </ul>
        </div>
    </div>
</nav>
<!-- FIN Menú de navegación -->

";
        // line 92
        $this->displayBlock('contenido', $context, $blocks);
        // line 93
        echo "<!-- Footer -->
<div class=\"float-none\"></div>
<footer id=\"pe\" class=\"py-5 bg-dark fixed-bottom\">
    <div class=\"container\">
        <p class=\"m-0 text-center text-white\">Copyright &copy; Cafés Mauro 2018</p>
    </div>
    <!-- /.container -->
</footer>

";
        // line 103
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/bootstrap/js/bootstrap.bundle.min.js"), "html", null, true);
        echo "\"></script>
";
        // line 105
        $this->displayBlock('javascripts', $context, $blocks);
        // line 106
        echo "</body>
</html>
";
    }

    // line 5
    public function block_titulo($context, array $blocks = array())
    {
        echo "Cafés Mauro";
    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 92
    public function block_contenido($context, array $blocks = array())
    {
    }

    // line 105
    public function block_javascripts($context, array $blocks = array())
    {
        echo " ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 105,  239 => 92,  234 => 12,  228 => 5,  222 => 106,  220 => 105,  216 => 104,  211 => 103,  200 => 93,  198 => 92,  190 => 86,  184 => 83,  181 => 82,  174 => 79,  171 => 77,  164 => 73,  160 => 72,  154 => 68,  146 => 63,  142 => 62,  138 => 61,  134 => 60,  129 => 58,  125 => 57,  119 => 53,  116 => 52,  113 => 51,  107 => 46,  103 => 45,  99 => 44,  88 => 36,  82 => 33,  70 => 24,  59 => 15,  54 => 13,  52 => 12,  47 => 10,  42 => 9,  37 => 7,  33 => 5,  27 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\base.html.twig");
    }
}
