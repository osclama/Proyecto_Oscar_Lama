<?php

/* gestionCafes/edicionCafes.html.twig */
class __TwigTemplate_3a4837aa2b3576a302773901e02b700c6b6fadc3374f35a6a7733554346b247c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "gestionCafes/edicionCafes.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_titulo($context, array $blocks = array())
    {
        echo "Listado de Categorías ";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        // line 9
        echo "    <div class=\"container\">
        ";
        // line 11
        echo "        <h2 class=\"mt-4 mb-3\">Listado de Cafés</h2>
        ";
        // line 13
        echo "        <div class=\"row\">
            <div class=\"col-md-2\">
            </div>
            <div class=\"col-md-8\">
                <table class=\"table\">
                    <thead class=\"thead-dark\">
                    <tr style=\"text-align: center\">
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">Foto</th>
                        <th scope=\"col\">Nombre</th>
                        <th scope=\"col\">Editar</th>
                        <th scope=\"col\">Borrar</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 28
        $context["contador"] = 0;
        // line 29
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cafes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cafe"]) {
            // line 30
            echo "                        <tr style=\"text-align: center\">
                            ";
            // line 31
            $context["contador"] = (($context["contador"] ?? null) + 1);
            // line 32
            echo "                            <th scope=\"row\">";
            echo twig_escape_filter($this->env, ($context["contador"] ?? null), "html", null, true);
            echo "</th>
                            ";
            // line 34
            echo "                            <td>
                                <img src=";
            // line 35
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((($context["fotosCafes"] ?? null) . twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 36
            echo "                                     width=\"200\" alt=\"\">
                            </td>
                            <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "nombre", array()), "html", null, true);
            echo "</td>
                            ";
            // line 40
            echo "                            ";
            // line 41
            echo "                            <td><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevoCafe", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\">
                                    <i class=\"fas fa-edit\"
                                       style=\"font-size: 130%\"></i>
                                </a>
                            </td>
                            ";
            // line 47
            echo "                            <td><a href=\"#\"
                                   onClick=\"return controlBorrado('";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("borrarCafe", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "')\">
                                    <i class=\"far fa-trash-alt\"
                                       style=\"font-size: 140%\"></i>
                                </a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cafe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    </tbody>
                </table>
            </div>
            <div class=\"col-md-2\">
            </div>
        </div>
    </div>
";
    }

    // line 63
    public function block_javascripts($context, array $blocks = array())
    {
        // line 64
        echo "    <script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>
    <script src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/controlBorrado.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "gestionCafes/edicionCafes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 65,  143 => 64,  140 => 63,  129 => 55,  116 => 48,  113 => 47,  104 => 41,  102 => 40,  98 => 38,  94 => 36,  88 => 35,  85 => 34,  80 => 32,  78 => 31,  75 => 30,  70 => 29,  68 => 28,  51 => 13,  48 => 11,  45 => 9,  43 => 8,  40 => 7,  34 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "gestionCafes/edicionCafes.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\gestionCafes\\edicionCafes.html.twig");
    }
}
