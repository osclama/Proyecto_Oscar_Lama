<?php

/* frontal/registro.html.twig */
class __TwigTemplate_66c3f44992b2343cb14488db1c7c07ddef3767e354b0b2fd8ded83a7cfea9b5f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/registro.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["formularioUsuario"] ?? null), array(0 => "form/cafeForm.html.twig"), true);
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_titulo($context, array $blocks = array())
    {
        echo "Registro Usuario ";
    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        // line 11
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\"> Cafés Mauro
            <small>Registro de usuario</small>
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Registro</li>
        </ol>
        <!-- Intro Content -->

        <div class=\"row\">
            <div class=\"col-lg-6\">
                ";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioUsuario"] ?? null), 'form_start');
        echo "
                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["formularioUsuario"] ?? null), 'widget');
        echo "
                ";
        // line 30
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioUsuario"] ?? null), 'form_end');
        echo "
            </div>
            <div class=\"col-lg-6\">
                <h3>¿Ya estás registrado?</h3>
                <p>Si ya tienes una cuenta de usuario y quieres volver a la página previa no tienes más que pulsar en <a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logado");
        echo "\"><strong>LOGIN</strong></a></p>
                <h4>Advertencia Importante</h4>
                <p>Asegúrese de registrar un tlf de contacto válido, en caso de no poder contactar con usted para confirmar la reserva nos reservamos el derecho de cancelar esta.</p>
            </div>
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "frontal/registro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 34,  77 => 30,  73 => 29,  69 => 28,  58 => 20,  47 => 11,  45 => 10,  42 => 9,  36 => 6,  32 => 2,  30 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/registro.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\frontal\\registro.html.twig");
    }
}
