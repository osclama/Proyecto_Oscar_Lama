<?php

/* frontal/categoria.html.twig */
class __TwigTemplate_0c7993809690f619c28d36e954843be6e876fc28079052d168aeb1980f07ae89 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/categoria.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Categorías ";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Categorías
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"index.html\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Datos de la Categoría</li>
        </ol>

        <!-- Portfolio Item Row -->
        <div class=\"row\">

            <div class=\"col-md-8\">
                ";
        // line 27
        echo "                <img class=\"img-fluid\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((($context["fotosCategorias"] ?? null) . twig_get_attribute($this->env, $this->source, ($context["datosCategoria"] ?? null), "foto", array()))), "html", null, true);
        echo "\" alt=\"\">
            </div>

            <div class=\"col-md-4\">
                <h3 class=\"my-3\">";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datosCategoria"] ?? null), "nombre", array()), "html", null, true);
        echo "</h3>
                <p>";
        // line 32
        echo twig_get_attribute($this->env, $this->source, ($context["datosCategoria"] ?? null), "descripcion", array());
        echo "</p>
            </div>

        </div>
    </div>
    <!-- /.container -->

";
    }

    public function getTemplateName()
    {
        return "frontal/categoria.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 32,  70 => 31,  62 => 27,  42 => 7,  39 => 6,  33 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/categoria.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\frontal\\categoria.html.twig");
    }
}
