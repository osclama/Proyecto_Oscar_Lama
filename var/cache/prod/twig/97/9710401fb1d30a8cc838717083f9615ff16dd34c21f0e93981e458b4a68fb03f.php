<?php

/* frontal/index.html.twig */
class __TwigTemplate_1b05263ca9319b3ca98df067c1a198ba0762cf74d5781560c2ed594a88c40c0a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/index.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo " Cafés Mauro ";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "    <header>
        <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
            <ol class=\"carousel-indicators\">
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>
            </ol>
            <div class=\"carousel-inner\" role=\"listbox\">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class=\"carousel-item active\" style=\"background-image: url(";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/slide01.jpg"), "html", null, true);
        echo ")\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>La calidad nuestra razón de ser</p>
                    </div>
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class=\"carousel-item\" style=\"background-image: url(";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/slide02.jpg"), "html", null, true);
        echo ")\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>Desde 1950</p>
                    </div>
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class=\"carousel-item\" style=\"background-image: url(";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/slide03.jpg"), "html", null, true);
        echo ")\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>Especialistas en Café</p>
                    </div>
                </div>
            </div>
            <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
                <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
                <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
        </div>
    </header>
    <div class=\"container\">
        <!-- Portfolio Section -->
        <h1 class=\"my-4\">Nuestras Recomentaciones</h1>
        ";
        // line 51
        echo "        <div class=\"row\">
            ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cafes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cafe"]) {
            // line 53
            echo "                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        ";
            // line 57
            echo "                        ";
            // line 59
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\"><img class=\"card-img-top\"
                                                                          src=";
            // line 60
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((($context["fotosCafes"] ?? null) . twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 61
            echo "                                                                          alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                ";
            // line 65
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "nombre", array()), "html", null, true);
            echo "</a>
                            </h4>
                            <p class=\"card-text\">";
            // line 67
            echo twig_get_attribute($this->env, $this->source, $context["cafe"], "caracteristicas", array());
            echo "</p>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cafe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "        </div>
        <!-- /.container -->

        <!-- Paginacion -->
        ";
        // line 77
        echo "        ";
        if ((($context["numTotalPaginas"] ?? null) > 1)) {
            // line 78
            echo "            <ul class=\"pagination justify-content-center\">
                ";
            // line 80
            echo "                ";
            if ((($context["paginaActual"] ?? null) > 1)) {
                // line 81
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\" href=\"";
                // line 82
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => ((((($context["paginaActual"] ?? null) - 1) < 1)) ? (1) : ((($context["paginaActual"] ?? null) - 1))))), "html", null, true);
                echo "\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                ";
            }
            // line 89
            echo "                ";
            // line 90
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($context["numTotalPaginas"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["indice"]) {
                // line 91
                echo "                    ";
                if (($context["indice"] == ($context["paginaActual"] ?? null))) {
                    // line 92
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"";
                    // line 94
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                } else {
                    // line 97
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"";
                    // line 98
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                }
                // line 101
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 102
            echo "                ";
            // line 103
            echo "                ";
            if ((($context["paginaActual"] ?? null) < ($context["numTotalPaginas"] ?? null))) {
                // line 104
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 106
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => ((((($context["paginaActual"] ?? null) + 1) <= ($context["numTotalPaginas"] ?? null))) ? ((($context["paginaActual"] ?? null) + 1)) : (($context["paginaActual"] ?? null))))), "html", null, true);
                echo "\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                ";
            }
            // line 113
            echo "            </ul>
        ";
        }
        // line 115
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "frontal/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 115,  230 => 113,  220 => 106,  216 => 104,  213 => 103,  211 => 102,  205 => 101,  197 => 98,  194 => 97,  186 => 94,  182 => 92,  179 => 91,  174 => 90,  172 => 89,  162 => 82,  159 => 81,  156 => 80,  153 => 78,  150 => 77,  144 => 72,  133 => 67,  125 => 65,  120 => 61,  114 => 60,  109 => 59,  107 => 57,  103 => 53,  99 => 52,  96 => 51,  73 => 30,  63 => 23,  53 => 16,  42 => 7,  39 => 6,  33 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/index.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\app\\Resources\\views\\frontal\\index.html.twig");
    }
}
