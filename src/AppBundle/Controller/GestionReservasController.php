<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Reserva;
use AppBundle\Entity\Usuario;
use AppBundle\Form\ReservaType;


/**
 *  El security dejará redirigir a esta zona a los usuarios con rol ROLE_USER
 * @Route("/reservas")
 */
class GestionReservasController extends Controller
{
    /**
     *Crea una nueva entrada en la lista de reservas, que pertenecerá al usuario logueado, o actualiza una existente cuando se introduce su id
     * como por ejemplo si seleccionamos editar en el botón correspondiente de la lista de reservas
     *
     *
     * @Route("/nueva/{id}", name="nuevaReserva")
     */
    public function nuevaReservaAction(Request $request, $id = null)
    {
//        Si se le pasa un id actualizaremos una reserva existente,
//         si no se le pasa entonces procederá a crear una nueva reserva

        if ($id) {
            $repository = $this->getDoctrine()->getRepository(Reserva::class);
//        filtramos la lista de reservas por el usuario logado, para eso pasamos usamos la entity Usuario y el método
            $reserva = $repository->find($id);

        } else {
            $reserva = new Reserva();
        }
        $formularioReserva = $this->createForm(ReservaType::class, $reserva);
        $formularioReserva->handleRequest($request);
        $usuario = $request->get('usuario');

//        var_dump($usuario);
//        var_dump($id);

        if ($formularioReserva->isSubmitted() && $formularioReserva->isValid()) {
            if(!$usuario){
                $control="usuario normal";
                $usuario = $this->getUser();
            }else{
                $control=null;
                $usuario=$user = $this->getDoctrine()
                    ->getRepository(Usuario::class)
                    ->findOneById($usuario);
            }
            $reserva->setUsuario($usuario);
            //Guardamos los datos
            $em = $this->getDoctrine()->getManager();
            $em->persist($reserva);
            $em->flush();
//          Redirigiremos a la ruta reservas, que es un listado de reservas
            if (!$control){
                return $this->redirectToRoute('edicionReservas');
            }else{
                return $this->redirectToRoute('reservas');
            }
        }
        return $this->render('gestionReservas/nuevaReserva.html.twig', array('formularioReserva' => $formularioReserva->createView()));

    }

    /**
     * Action que saca un listado de reservas del usuario logado
     *
     * @Route("/reservas", name="reservas")
     */
    public function reservasAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Reserva::class);
//        filtramos la lista de reservas por el usuario logado, para eso pasamos usamos la entity Usuario y el método
        $reservas = $repository->findByUsuario($this->getUser());
        return $this->render('gestionReservas/reservas.html.twig', array("reservas" => $reservas));
    }

    /**
     * Action que borra la reserva seleccionada por su id, una vez borrada redirige nuevamente al listado de reservas
     * del usuario. Si el usuario logado es el administrador redirigirá a otra página con el listado de todas las reservas
     * de los usuarios
     *
     * @Route("/borrar/{id}", name="borrarReserva")
     */
    public function borrarReservaAction(Request $request, $id = null)
    {
//        Búsqueda de la reserva
        $repository = $this->getDoctrine()->getRepository(Reserva::class);
        $reserva = $repository->find($id);
//        Borrado de la reserva
        $em = $this->getDoctrine()->getManager();
        $em->remove($reserva);
        $em->flush();
//        Una vez realizada la eliminación de la reserva se vuelve a generar el listado de las reservas
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('reservas');
        }else{
            return $this->redirectToRoute('edicionReservas');
        }

    }
}