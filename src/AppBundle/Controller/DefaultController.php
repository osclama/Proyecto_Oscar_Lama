<?php

namespace AppBundle\Controller;

use AppBundle\Form\UsuarioType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

//para poder usar la tabla Cafés hay que incorporarla
use AppBundle\Entity\Cafes;
//lo mismo sucede con la tabla Categoria
use AppBundle\Entity\Categoria;
use AppBundle\Entity\Usuario;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;


class DefaultController extends Controller
{
    /**
     * homepage será el nombre de la ruta principal, que redirige hacia index, pasándole como parámetro el número
     * de página, que listará los cafés top que tengamos en nuestra base de datos almacenados de tres en tres en cada
     * página. Esta página tendrá un slider central de 3 imágenes en su parte superior
     * @Route("/{pagina}", requirements={"pagina": "\d+"}, name="homepage")
     */
    public function homeAction(Request $request, $pagina = 1)
    {
        //Para manejar los datos dentro de getRepository se referencia a la entidad deseada.
        // En este caso se capturan los datos de la tabla Cafes de la DDBB
        $cafesRepository = $this->getDoctrine()->getRepository(Cafes::class);
        $numCafes = 3; //es el número de cafés top que queremos listar en la homepage

//        Esto se ha implementado en el repositorio de cafes para hacer la paginación una función global
        //Se devuelve un array de objetos llamado datosCafes, de tipo Cafes, para devolver todos
        //se pondría findAll en su lugar. En este caso se filtran los datos por los mejores cafés,
        // definidos en el campo Top de la tabla con valor 1 (el campo de la tabla se pone a continuación
        //de findBy en mayúsculas)
//        $datosCafes = $cafesRepository->findByTop(1);
        //para comprobar que hemos capturado todos los datos podemos descomentar la siguiente linea
        //var_dump($datosCafes);

        // createQueryBuilder() permite personalizar las consultas, se le pasa el repositorio creado, y se guardan los datos
        //en un alias de la clase, en nuestro caso es c
//
        $numTotalPaginas = $cafesRepository->getNumeroPaginasCafesTop($numCafes);
        $paginaCafesTop = $cafesRepository->getPaginaCafesTop($pagina, $numCafes);
        //a continuación se pasa a la plantilla los datos capturados, y el número de página en la que nos encontramos
        return $this->render('frontal/index.html.twig', array('cafes' => $paginaCafesTop, 'paginaActual' => $pagina, 'numTotalPaginas' => $numTotalPaginas));
    }

    /**
     * nosotros será la ruta que visualizará los distintos sitios con información sobre la empresa, dentro de ella pulsando en
     * los botones correspondientes de cada ficha se rendizará una u otra página. Las páginas disponibles son:
     * Gerencia, Historia, y Equipo
     *
     *  Matches /nosotros/*
     * @Route("/nosotros/{ruta}", name="nosotros")
     */
    public function nosotrosAction(Request $request, $ruta = null)
    {
        if ($ruta != null) {
            if ($ruta === "gerencia") {
                return $this->render('frontal/gerencia.html.twig');
            } elseif ($ruta === "equipo") {
                return $this->render('frontal/equipo.html.twig');
            } elseif ($ruta === "historia") {
                return $this->render('frontal/historia.html.twig');
            } else {
                return $this->render('frontal/nosotros.html.twig');
            }
        }
        return $this->render('frontal/nosotros.html.twig');
    }

    /**
     * @Route("/contactar/{sitio}", name="contactar")
     */
    public function contactarAction(Request $request, $sitio = "todos")
    {

        return $this->render('frontal/locales.html.twig', array("sitio" => $sitio));
    }

    /**
     * Action que se usa para ver el detalle de cada cafe cuando lo seleccionamos dentro de los cafés de una categoría
     * Se define como null por defecto el valor del parámetro si visito esta ruta sin darle valor al id, por lo que no
     * se mostrará ninguno por defecto
     *  Matches /cafes/*
     * @Route("/cafes/{id}", name="cafes")
     */
    public function cafeAction(Request $request, $id = null)
    {
        if ($id != null) {
            $cafesRepository = $this->getDoctrine()->getRepository(Cafes::class);
            $datosCafe = $cafesRepository->find($id);
            return $this->render('frontal/cafes.html.twig', array("datosCafe" => $datosCafe));

        } else {
            //Si no se le pasa ningún parámetro en la ruta listará todos los cafés
            return $this->redirectToRoute('todosNuestrosCafes');
        }
    }
    //*******************************************************************************************

    /**
     * Action que se usa para listar todos los cafés guardados en la DDBB. Se ejecuta cuando se selecciona
     * cafes en la ruta inicio/categorias/cafes/Detalle del Cafe
     *
     * Matches /todosNuestrosCafes/*
     * @Route("/todosNuestrosCafes/{pagina}", requirements={"pagina": "\d+"}, name="todosNuestrosCafes")
     */
    public function todosNuestrosCafesAction(Request $request, $pagina = 1)
    {
        $cafesRepository = $this->getDoctrine()->getRepository(Cafes::class);
        $numCafes = 6; //es el número de cafés que queremos listar en la homepage

        $numTotalPaginas = $cafesRepository->getNumeroPaginasTodosCafes($numCafes);
        $paginaCafes = $cafesRepository->getPaginaTodosCafes($pagina, $numCafes);
        //a continuación se pasa a la plantilla los datos capturados, y el número de página en la que nos encontramos
        return $this->render('frontal/listadoCompletoCafes.html.twig', array('datosCafes' => $paginaCafes, 'paginaActual' => $pagina, 'numTotalPaginas' => $numTotalPaginas));
    }

    //*******************************************************************************************

    /**
     * Action que se ejecuta al seleccionar en el menú superior la pestaña Cafés, o en la barra de navegación superior Categorías, cuando tenemos una ruta por ejemplo
     * Inicio/Categorias/Cafes
     *
     * Matches /listadoCategorias/*
     * @Route("/listadoCategorias/{pagina}", requirements={"pagina": "\d+"}, name="listadoCategorias")
     */
    public function listadoCategoriasAction(Request $request, $pagina = 1)
    {
        $categoriaRepository = $this->getDoctrine()->getRepository(Categoria::class);
        $numCategorias = 6; //es el número de cafés top que queremos listar en la homepage

        $numTotalPaginas = $categoriaRepository->getNumeroPaginasCategorias($numCategorias);
        $paginaCategorias = $categoriaRepository->getPaginaCategorias($pagina, $numCategorias);
        //a continuación se pasa a la plantilla los datos capturados, y el número de página en la que nos encontramos
        return $this->render('frontal/listadoCategorias.html.twig', array('datosCategorias' => $paginaCategorias, 'paginaActual' => $pagina, 'numTotalPaginas' => $numTotalPaginas));
    }

    /**
     * Action que se ejecuta cuando seleccionamos una categoría de listado de categorías. El action
     * recoge todos los cafés disponibles en esa categoría, y los muestra en distintas páginas
     *
     * Matches /listadoCafes/*
     * @Route("/{categoria}/listadoCafes/{pagina}", name="listadoCafes")
     */
    public function listadoCafesAction(Request $request, $categoria, $pagina = 1)
    {
//        $cafesRepository = $this->getDoctrine()->getRepository(Cafes::class);
//        $numCafes = 6; //es el número de cafésque queremos listar en la página
//        $numTotalPaginas = $cafesRepository->getNumeroPaginasCafes($numCafes,$categoria);
//        $paginaCafes = $cafesRepository->getPaginaCafes($page, $numCafes,$categoria);
//        return $this->render('frontal/listadoCafes.html.twig', array('datosCafes' => $paginaCafes, 'paginaActual' => $page, 'numTotalPaginas' => $numTotalPaginas));

        //OTRO MÉTODO QUE FUNCIONA, SIN ACCEDER USAR LAS CONSULTAS PERSONALIZADAS DENTRO DEL REPOSITORIO
//        Obtenemos el número Total de páginas que hay dentro de la categoría, depende del $numCafes que queremos ver por página
        $numCafes = 6; //es el número de cafésque queremos listar en la página
        $nombreCategoria = $request->get('nombreCategoria');
        $em = $this->getDoctrine()->getManager();
        $consulta = $em->createQuery('
        SELECT count(c.id)/(:numCafes) FROM AppBundle:Cafes c 
        JOIN c.categorias u 
        WHERE u.id = :categoria
        ');
        $consulta->setParameter('numCafes', $numCafes);
        $consulta->setParameter('categoria', $categoria);
        $numTotalPaginas = ceil($consulta->getResult()[0][1]);


//      A continuación recuperamos el array con los datos de los cafés de cada página
        $em = $this->getDoctrine()->getManager();
        $dql = '
        SELECT c FROM AppBundle:Cafes c 
        JOIN c.categorias u 
        WHERE u.id = :categoria       
        ';
        $consulta = $em->createQuery($dql);
        $consulta->setMaxResults(1);
        $consulta->setParameter('categoria', $categoria);
        $consulta->setFirstResult($numCafes * ($pagina - 1));
        $consulta->setMaxResults($numCafes);
        $paginaCafes = $consulta->getResult();


//        Los datos obtenidos de las dos consultas se los pasamos a la página que contiene la vista para renderizar los resultados
        return $this->render('frontal/listadoCafes.html.twig', array('datosCafes' => $paginaCafes, 'paginaActual' => $pagina, 'numTotalPaginas' => $numTotalPaginas, 'categoria' => $categoria, 'nombreCategoria' => $nombreCategoria));

    }

//    public function listadoCafesAction(Request $request, $categoria, $page = 1)
//    {
//        $cafesRepository = $this->getDoctrine()->getRepository(Cafes::class);
//        $numCafes = 6; //es el número de cafésque queremos listar en la página
//        $numTotalPaginas = $cafesRepository->getNumeroPaginasCafes($numCafes);
//        $paginaCafes = $cafesRepository->getPaginaCafes($page, $numCafes);
//        return $this->render('frontal/listadoCafes.html.twig', array('datosCafes' => $paginaCafes, 'paginaActual' => $page, 'numTotalPaginas' => $numTotalPaginas));
//
//    }

    //*******************************************************************************************

    /**
     * Action que se usa para ver el detalle de cada categoría cuando la seleccionamos
     * Se define como null por defecto el valor del parámetro si visito esta ruta sin darle valor al id, por lo que no
     * se mostrará nada por defecto, y redigirá a la página de inicio
     *
     * @Route("/categoria/{id}", name="categoria")
     */
    public function categoriaAction(Request $request, $id = null)
    {
        if ($id != null) {
            $categoriaRepository = $this->getDoctrine()->getRepository(Categoria::class);
            $datosCategoria = $categoriaRepository->find($id);
            return $this->render('frontal/categoria.html.twig', array("datosCategoria" => $datosCategoria));
        } else {
            //Si no se le pasa ningún parámetro en la ruta redirige a la pantalla de inicio
            return $this->redirectToRoute('homepage');
        }
    }

//    ******************************************************************************
//      Registro de Usuarios
    /**
     * Crea nuevas entradas en usuarios
     * @Route("/registro/", name="registro")
     */
    public function registroAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $usuario = new Usuario();
        //Se crea un nuevo formulario a través del método de la clase CafeType
//        para la validación se usan los grupos default y registro, por lo que si se deja vacía la
//        contraseña dará un error de validación
        $formularioUsuario = $this->createForm(UsuarioType::class, $usuario,
            array('validation_groups' => array('default', 'registro')));
        //A continuación se recoge la información del formlario
        $formularioUsuario->handleRequest($request);
        //si el formulario ha sido enviado y es válida entraría dentro del if
        if ($formularioUsuario->isSubmitted() && $formularioUsuario->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($usuario, $usuario->getPlainPassword());
            $usuario->setPassword($password);
            // 3b) $username=$email
            $usuario->setUsername($usuario->getEmail());
            //3c) $roles
            $usuario->setRoles(array('ROLE_USER'));
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($usuario);
            $entityManager->flush();
//            página 226 libro symfony
            $this->addFlash('info', '¡Enhorabuena! Te has registrado correctamente');
            return $this->redirectToRoute('logado');
        }
        return $this->render('frontal/registro.html.twig', array('formularioUsuario' => $formularioUsuario->createView()));
    }

    /**
     * Permite entrar en la sesión de usuario de usuarios registrados
     * @Route("/login/", name="logado")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('frontal/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }
}
