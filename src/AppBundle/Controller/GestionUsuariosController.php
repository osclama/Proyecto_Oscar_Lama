<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Reserva;
use AppBundle\Entity\Usuario;
use AppBundle\Form\ReservaType;
use AppBundle\Form\UsuarioType;
use AppBundle\Form\EdicionUsuarioType;


/**
 *  Ruta solo accesible por el usuario con rol ROLE_ADMIN, definido así en security.yml
 *
 * @Route("/usuarios")
 */
class GestionUsuariosController extends Controller
{
    /**
     *Action que saca una lista de todas las reservas realizadas por usuarios, permitiendo borrarlas o modificarlas
     *
     * @Route("/edicionReservas", name="edicionReservas")
     */
    public function edicionReservasAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Reserva::class);
//        listamos todas las reservas gravadas por usuarios
        $reservas = $repository->findAll();
        return $this->render('gestionCafes/edicionReservas.html.twig', array("reservas" => $reservas));
    }

    /**
     * Action que saca una lista con todos los usuarios de la app, permitiendo borrarlos o editarlos
     *
     * @Route("/edicionUsuarios", name="edicionUsuarios")
     */
    public function edicionUsuariosAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
//        filtramos la lista de reservas por el usuario logado, para eso pasamos usamos la entity Usuario y el método
//        $usuarios = $repository->findAll();
//        se ordena por el nombre completo en orden ascendente
        $usuarios = $repository->findBy([], ['nombreCompleto' => 'ASC']);

        return $this->render('gestionCafes/edicionUsuarios.html.twig', array("usuarios" => $usuarios));
    }
    /**
     * Action que permite editar los datos de un usuario seleccionado
     *
     * @Route("/edicionDatosUsuarios/{id}", name="edicionDatosUsuarios")
     */
    public function edicionDatosUsuariosAction(Request $request, $id = null)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
        $usuario = $repository->find($id);

        //Se crea un nuevo formulario a través del método de la clase EdicionUsuarioType
        $formularioUsuario = $this->createForm(EdicionUsuarioType::class, $usuario);
        //A continuación se recoge la información del formlario
        $formularioUsuario->handleRequest($request);
        //si el formulario ha sido enviado y es válida entraría dentro del if
        if ($formularioUsuario->isSubmitted() && $formularioUsuario->isValid() ) {

            $usuario->setUsername($usuario->getEmail());

            $entityManager = $this->getDoctrine()->getManager();

            //$entityManager->persist($usuario);
            $entityManager->flush();

            if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                return $this->redirectToRoute('homepage');
            } else {
                return $this->redirectToRoute('edicionUsuarios');
            }
        }
        return $this->render('gestionCafes/edicionDatosUsuarios.html.twig', array('formularioUsuario' => $formularioUsuario->createView()));
    }

    /**
     *  Action que permite borrar un usuario seleccionado por su id
     * @Route("/borrarUsuario/{id}", name="borrarUsuario")
     */
    public function borrarUsuarioAction(Request $request, $id = null)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
        $usuario = $repository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($usuario);
        $em->flush();
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        } else {
            return $this->redirectToRoute('edicionUsuarios');
        }
    }
}