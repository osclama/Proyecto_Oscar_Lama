<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Reserva
 *
 * @ORM\Table(name="reserva")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReservaRepository")
 */
class Reserva
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

//    Se realiza una validación en servidor mediante los Asserts. Hay que incluir la clase correspondiente para que funcione
    /**
     * @var int
     *
     * @ORM\Column(name="asistentes", type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 10,
     *      minMessage = "La reserva debe tener un mínimo de 1 asistente.",
     *      maxMessage = "La reserva no puede tener más de 10 asistentes."
     * )
     */
    private $asistentes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="text", nullable=true)
     */
    private $observaciones;

    /**
     * Muchas reservas distintas pueden tener un único usuario, un usuario puede tener muchas reservas distintas
     * pero cada una de esas reservas solo tiene un usuario
     *
     * reservas es un array que va a estar en usuario. usuario_id va a ser el campo de la tabla Reserva
     * que va a guardar los ids de los usuarios para relacionarlos con la tabla usuario y su campo id
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="reservas")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Reserva
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set asistentes.
     *
     * @param int $asistentes
     *
     * @return Reserva
     */
    public function setAsistentes($asistentes)
    {
        $this->asistentes = $asistentes;

        return $this;
    }

    /**
     * Get asistentes.
     *
     * @return int
     */
    public function getAsistentes()
    {
        return $this->asistentes;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Reserva
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set usuario.
     *
     * @param \AppBundle\Entity\Usuario|null $usuario
     *
     * @return Reserva
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return \AppBundle\Entity\Usuario|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
