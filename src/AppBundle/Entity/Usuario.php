<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsuarioRepository")
 * @UniqueEntity(
 * fields={"email"},
 * errorPath="email",
 * message="Ya existe una cuenta de usuario con ese email, inténtelo con otro distinto"
 *)
 */
class Usuario implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $username;
    /**
     * @Assert\Regex(
     *     pattern = "/^[a-zÁÉÍÓÚñáéíóúÑ\s]+$/i",
     *     message="El nombre no puede contener números ni símbolos."
     * )
     *
     * @Assert\Length(
     *      min = 9,
     *      max = 100,
     *      minMessage = "La longitud del Nombre debe de tener un mínimo de 9 caracteres",
     *      maxMessage = "La longitud del Nombre debe de tener un máximo de 9 caracteres",
     * )
     * @ORM\Column(type="string", length=254)
     */
    private $nombreCompleto;

    /**
     * @Assert\Length(
     *      min = 9,
     *      max = 9,
     *      minMessage = "La longitud del teléfono debe de tener un mínimo de 9 caracteres",
     *      maxMessage = "La longitud del teléfono debe de tener un máximo de 9 caracteres",
     *      exactMessage = "La longitud del teléfono debe de tener exactamente 9 caracteres"
     * )
     * @Assert\Type(
     *     type="numeric",
     *     message="El valor {{ value }} no es válido, debe ser numérico."
     * )
     * @ORM\Column(type="string", length=254)
     */
    private $telefono;
//    Se añade grupo de validación para poder usar la funcionalidad de editar los datos de usuario
//    por el administrador
    /**
     *
     * @Assert\NotBlank(
     *     groups={"registro"},
     *      message = "El Campo password no puede estar vacío"
     * )
     * @Assert\Length(
     *      min = 6,
     *      max = 20,
     *      minMessage = "La longitud del password debe de tener un mínimo de 6 caracteres",
     *      maxMessage = "La longitud del password debe de tener un máximo de 20 caracteres",
     * )
     */
    private $plainPassword;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @Assert\Email(
     *      message = "El email '{{ value }}' no es un email valido.",
     *      checkMX = true
     * )
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
//    ************************************************************************************
//    Se añade el Rol para poder restringir el acceso a determinadas áreas de la aplicación
    /**
     * @ORM\Column(type="string", length=256)
     */
    private $roles;

//    ***********************************************************************************

    /**
     * Se añade la variable reservas, que es un array que relaciona a cada usuario con las reservas
     * que tiene hechas. Se añade al constructor el ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Reserva", mappedBy="usuario")
     */
    private $reservas;


    public function __construct()
    {
        $this->isActive = true;
        $this->reservas = new ArrayCollection();

        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;
    }

    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }

    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        return $this->password = $password;
    }

// Los roles serán almacenados y recuperados a través del formato json

    public function getRoles()
    {
        $roles = json_decode($this->roles);
        return $roles;
    }

    public function setRoles($roles)
    {
        $roles_json = json_encode($roles);
        return $this->roles = $roles_json;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return Usuario
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add reserva.
     *
     * @param \AppBundle\Entity\Reserva $reserva
     *
     * @return Usuario
     */
    public function addReserva(\AppBundle\Entity\Reserva $reserva)
    {
        $this->reservas[] = $reserva;

        return $this;
    }

    /**
     * Remove reserva.
     *
     * @param \AppBundle\Entity\Reserva $reserva
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeReserva(\AppBundle\Entity\Reserva $reserva)
    {
        return $this->reservas->removeElement($reserva);
    }

    /**
     * Get reservas.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservas()
    {
        return $this->reservas;
    }
}
