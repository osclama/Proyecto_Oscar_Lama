<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Categoria
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="foto", type="string", length=255)
     */
    private $foto;

    /**
     * Se establece una relación manytomany entre categoría y la tabla Cafes
     * En la tabla cafes el campo que hace referencia a esta relacion es el llamado categorias
     * en Categoria cafes va a ser una variable que guardará en un array los distintos cafes
     * que tiene cada categoria
     * @ORM\ManyToMany(targetEntity="Cafes", mappedBy="categorias")
     */
    private $cafes;

    // los cafés va a ser un nuevo objeto de tipo arraycollection, ya que va a tener múltiples entradas de cafes dentro de cada categoria
    public function __construct()
    {
        $this->cafes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Categoria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set foto
     *
     * @param string $foto
     *
     * @return Categoria
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Add cafes
     *
     * @param \AppBundle\Entity\Cafes $cafes
     *
     * @return Categoria
     */
    public function addCafe(\AppBundle\Entity\Cafes $cafes)
    {
        $this->cafes[] = $cafes;

        return $this;
    }

    /**
     * Remove cafes
     *
     * @param \AppBundle\Entity\Cafes $cafes
     */
    public function removeCafe(\AppBundle\Entity\Cafes $cafes)
    {
        $this->cafes->removeElement($cafes);
    }

    /**
     * Get cafes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCafes()
    {
        return $this->cafes;
    }
    //método mágico para convertir los objetos categora a string, y devolver el nombre, para poder rellenar el select
    public function __toString(){
        return $this->nombre;
    }
}
