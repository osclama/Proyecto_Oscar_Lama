<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

    //el repositorio es la clase que se usa para las consultas
/**
 * Cafes
 *
 * @ORM\Table(name="cafes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CafesRepository")
 */
class Cafes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origen", type="string", length=255, nullable=true)
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="foto", type="string", length=255)
     */
    private $foto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="caracteristicas", type="text", nullable=true)
     */
    private $caracteristicas;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaCreacion", type="datetime", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var bool
     *
     * @ORM\Column(name="top", type="boolean")
     */
    private $top;

    // Se define una relación ManyToOne entre el café y su categoría
    // Un café puede tener una única categoría, y un categoría puede tener muchos cafés

    /**
     * inversedBy es el atributo dentro de la entity categoria al que corresponde la relación con la
     * entity cafes, este campo se ha llamado cafes en la tabla categoria.
     * categoria_id es el nombre de la columna que se crea en la tabla cafes,
     * e id es la columna de la tabla categoria
     * Se hace un join entre tablas a través de la columna llamada id.
     * @ORM\ManyToMany(targetEntity="Categoria", inversedBy="cafes")
     * @ORM\JoinTable(name="cafes_categorias")
     */
    private $categorias;
//-------------------------------------------------------------------------------
    // categoria va a ser un nuevo objeto de tipo arraycollection,
    // ya que va a tener múltiples entradas de cafes dentro de cada categoria
    public function __construct()
    {
        $this->categorias = new ArrayCollection();
    }


//--------------------------------------------------------------------------------
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre.
     *
     * @param string $nombre
     *
     * @return Cafes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Cafes
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set origen.
     *
     * @param string|null $origen
     *
     * @return Cafes
     */
    public function setOrigen($origen = null)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string|null
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set foto.
     *
     * @param string $foto
     *
     * @return Cafes
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto.
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set caracteristicas.
     *
     * @param string|null $caracteristicas
     *
     * @return Cafes
     */
    public function setCaracteristicas($caracteristicas = null)
    {
        $this->caracteristicas = $caracteristicas;

        return $this;
    }

    /**
     * Get caracteristicas.
     *
     * @return string|null
     */
    public function getCaracteristicas()
    {
        return $this->caracteristicas;
    }

    /**
     * Set fechaCreacion.
     *
     * @param \DateTime|null $fechaCreacion
     *
     * @return Cafes
     */
    public function setFechaCreacion($fechaCreacion = null)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion.
     *
     * @return \DateTime|null
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set top.
     *
     * @param bool $top
     *
     * @return Cafes
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top.
     *
     * @return bool
     */
    public function getTop()
    {
        return $this->top;
    }


    //método mágico para convertir los objetos categora a string, y devolver el nombre, para poder rellenar el select
    public function __toString(){
        return $this->nombre;
    }



    /**
     * Add categoria.
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Cafes
     */
    public function addCategoria(\AppBundle\Entity\Categoria $categoria)
    {
        $categoria->setParentMyItem($this); // Setting parent item
        $this->categorias->add($categoria);
        //$this->categorias[] = $categoria;

        return $this;
    }

    /**
     * Remove categoria.
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCategoria(\AppBundle\Entity\Categoria $categoria)
    {
        return $this->categorias->removeElement($categoria);
    }

    /**
     * Get categorias.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorias()
    {
        return $this->categorias;
    }
}
