<?php

namespace AppBundle\Form;


use AppBundle\AppBundle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

///para poder usar los tipos de campos del formulario hay que referenciar las clases de estos///
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//he instalado CKEditorBundle para poder implementar un editor de textos para los campos descripción y características
//una vez instalado para poder usarlo hay que referenciarlo
use FOS\CKEditorBundle\Form\Type\CKEditorType;
//para poder usar filetype hay que agregar la clase correspondiente
use Symfony\Component\Form\Extension\Core\Type\FileType;
//Para incluir el estilo en el selector de categorias se añade la clase EntityType
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CafeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //El  campo fechaCreacion no se va a recoger porque se va acrear automáticamente cuando creemos la entrada
        // ->add('fechaCreacion', DateType::class)
        $builder
            ->add('nombre', TextType::class)
//            Para poder customizar el selector de categorías se define como clase EntityType
            ->add('categorias', EntityType::class,array(
                'class'=>'AppBundle:Categoria',
                'multiple'=>true,
                'placeholder' => 'Seleccione opciones',
                'attr'=>array(
                    'size'=>'3'
                )

            ))
            ->add('descripcion', CKEditorType::class)
            ->add('origen', TextType::class)
            ->add('caracteristicas', CKEditorType::class)
//            En la documentación de Symfony en el apartado de FileType Field viene las opciones que se le pueden agregar
//            la propiedad attr nos permite agregar atributos a este elemento. Se le añade onchange que agrega al input de html esta propiedad
//            Cuando se produzca una acción se va a lanzar un evento que vamos a capturar con js
                //Hay que ponerle data_class=>null para que cuando editemos la entrada no de error
            ->add('foto', FileType::class, array('data_class' => null,'attr'=>array('onchange'=>'onChange(event)')))
            //top es de tipo boolean
            ->add('top')
            ->add('salvar', SubmitType::class, array('label' => 'Guardar'));

    }
}