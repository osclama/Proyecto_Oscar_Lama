<?php

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

///para poder usar los tipos de campos del formulario hay que referenciar las clases de estos///
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//he instalado CKEditorBundle para poder implementar un editor de textos para los campos descripción y características
//una vez instalado para poder usarlo hay que referenciarlo
use FOS\CKEditorBundle\Form\Type\CKEditorType;
//para poder usar filetype hay que agregar la clase correspondiente
use Symfony\Component\Form\Extension\Core\Type\FileType;

class CategoriaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //El  campo fechaCreacion no se va a recoger porque se va acrear automáticamente cuando creemos la entrada
        // ->add('fechaCreacion', DateType::class)
        $builder
            ->add('nombre', TextType::class)
            ->add('descripcion', CKEditorType::class)
//            En la documentación de Symfony en el apartado de FileType Field viene las opciones que se le pueden agregar
//            la propiedad attr nos permite agregar atributos a este elemento. Se le añade onchange que agrega al input de html esta propiedad
//            Cuando se produzca una acción se va a lanzar un evento que vamos a capturar con js
            ->add('foto', FileType::class, array('data_class' => null,'attr'=>array('onchange'=>'onChange(event)')))
//            ->add('coverFile', FileType::class, array('data_class' => null))
//            ->add('coverFile', FileType::class, array('data_class' => null,'required' => false))
            ->add('salvar', SubmitType::class, array('label' => 'Guardar'));

    }
}