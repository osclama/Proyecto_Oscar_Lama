<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

//para poder usar la tabla Cafés hay que incorporarla, igualmente la del formulario
use AppBundle\Entity\Cafes;
use AppBundle\Form\CafeType;
//lo mismo con categoria
use AppBundle\Entity\Categoria;
use AppBundle\Form\CategoriaType;

//Para la captura de errores
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Crea nuevas entradas en la tabla cafes
 * @Route("/gestionCafes")
 */
class GestionCafesController extends Controller
{


    /**
     * Crea nuevas entradas en la tabla cafes
     * @Route("/nuevoCafe/{id}", name="nuevoCafe")
     */
    public function nuevoCafeAction(Request $request, $id = null)
    {
        //Ejemplo simple de captura de los datos del formulario
        /*
        if (!is_null($request)){
            //si no es nulo el objeto request capturamos todos los datos del objeto
            $datosCapturados=$request->request->all();
            var_dump($datosCapturados);
        }
        */
        if ($id) {
//            var_dump($id);
            $repository = $this->getDoctrine()->getRepository(Cafes::class);
            $cafe = $repository->find($id);
        } else {
            //nuevo objeto de tipo entity
            $cafe = new Cafes();
        }

        //Se crea un nuevo formulario a través del método de la clase CafeType
        $formularioCafe = $this->createForm(CafeType::class, $cafe);
        //A continuación se recoge la información del formlario
        $formularioCafe->handleRequest($request);
        //si el formulario ha sido enviado y es válida entraría dentro del if
        if ($formularioCafe->isSubmitted() && $formularioCafe->isValid()) {
            //rellenamos el objeto cafe con los datos capturados del formulario
            $cafe = $formularioCafe->getData();

            $fotoFile = $cafe->getFoto();
            //para ver lo que está recogiendo el formulario en el campo foto, que es de tipo file

            //para almacenar el archivo en nuestro servidor primero generamos el nombre único, con su correspondiente extensión
            $fotoFileName = $this->generateUniqueFileName() . '.' . $fotoFile->guessExtension();
            //var_dump($fotoFile);
            // var_dump($fotoFileName);
            // Método para mover las fotos al directorio que está configurado en el config.yml
            try {
                $fotoFile->move(
                    $this->getParameter('fotosCafes_directory'),
                    $fotoFileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            //introducimos manualmente los datos no aportados por el formulario
            $cafe->setFoto($fotoFileName);
            $cafe->setFechaCreacion(new \DateTime());

            //A continuación se almacena la información en el entityManager
            $entityManager = $this->getDoctrine()->getManager();

            //salvamos la información temporalmente en el entityManager mediante persist
            $entityManager->persist($cafe);

            //ejecutamos las consultas contra la base de datos con flush()
            $entityManager->flush();

            //Al realizar la inserción correctamente redirige a la ruta con el detalle del café incluído,
            //para esto hay que pasarle mediante un array de valores el id del café introducido, que tendrá
            //el id actualizado tras la creación de la nueva entrada

            //para hacer pruebas renderizamos las vista test.html.twig
//            return $this->render('test/test.html.twig');
//            return $this->redirectToRoute('cafes', array('id' => $cafe->getId()));
            return $this->redirectToRoute('edicionCafes');
        }
        return $this->render('gestionCafes/nuevoCafe.html.twig', array('formularioCafe' => $formularioCafe->createView()));
    }

    /**
     * Crea nuevas entradas en la tabla categorias
     * @Route("/nuevaCategoria/{id}", name="nuevaCategoria")
     */
    public function nuevaCategoriaAction(Request $request, $id = null)
    {
        if ($id) {
//            var_dump($id);
            $repository = $this->getDoctrine()->getRepository(Categoria::class);
            $categoria = $repository->find($id);
        } else {
            $categoria = new Categoria();
        }
        $formularioCategoria = $this->createForm(CategoriaType::class, $categoria);
        $formularioCategoria->handleRequest($request);
        if ($formularioCategoria->isSubmitted() && $formularioCategoria->isValid()) {
            $categoria = $formularioCategoria->getData();
            $fotoFile = $categoria->getFoto();
            echo $fotoFile->guessExtension();
            $fotoFileName = $this->generateUniqueFileName() . '.' . $fotoFile->guessExtension();

            try {
                $fotoFile->move(
                    $this->getParameter('fotosCategorias_directory'),
                    $fotoFileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $categoria->setFoto($fotoFileName);
            //almacenamos la nueva categoria
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categoria);
            $entityManager->flush();
//            return $this->redirectToRoute('categoria', array('id' => $categoria->getId()));
            return $this->redirectToRoute('edicionCategorias');

        }
        return $this->render('gestionCafes/nuevaCategoria.html.twig', array('formularioCategoria' => $formularioCategoria->createView()));
    }


//    Para poder guardar el archivo de imagen en nuestro servidor hay que crear la siguiente función, que lo que hace es encriptar con md5 el
//    nombre del archivo para que sea único
    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * @Route("/edicionCafes", name="edicionCafes")
     */
    public function edicionCafesAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Cafes::class);
        $cafes = $repository->findAll();
        return $this->render('gestionCafes/edicionCafes.html.twig', array("cafes" => $cafes));
    }

    /**
     * @Route("/edicionCategorias", name="edicionCategorias")
     */
    public function edicionCategoriasAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Categoria::class);
        $categorias = $repository->findAll();
        return $this->render('gestionCafes/edicionCategorias.html.twig', array("categorias" => $categorias));
    }


    /**
     * @Route("/borrarCafe/{id}", name="borrarCafe")
     */
    public function borrarCafeAction(Request $request, $id = null)
    {
//        Búsqueda del café
        $repository = $this->getDoctrine()->getRepository(Cafes::class);
        $cafe = $repository->find($id);
//        Borrado de la categoria
        $em = $this->getDoctrine()->getManager();
        $em->remove($cafe);
        $em->flush();
//        Una vez realizada la eliminación del cafe se vuelve a generar el listado de cafes
        return $this->redirectToRoute('edicionCafes');

    }

    /**
     * @Route("/borrarCategoria/{id}", name="borrarCategoria")
     */
    public function borrarCategoriaAction(Request $request, $id = null)
    {
//        Búsqueda de la categoria
        $repository = $this->getDoctrine()->getRepository(Categoria::class);
        $categoria = $repository->find($id);
//        Borrado de la categoria
        $em = $this->getDoctrine()->getManager();
        $em->remove($categoria);
        $em->flush();
//        Una vez realizada la eliminación de la categoría se vuelve a generar el listado de las categorias
        return $this->redirectToRoute('edicionCategorias');

    }

}