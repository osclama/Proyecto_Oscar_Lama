<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Reserva;
use AppBundle\Entity\Usuario;
use AppBundle\Form\ReservaType;
use AppBundle\Form\UsuarioType;
use AppBundle\Form\EdicionUsuarioType;


/**
 * @Route("/usuarios")
 */
class GestionUsuariosController extends Controller
{
    /**
     * @Route("/edicionReservas", name="edicionReservas")
     */
    public function edicionReservasAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Reserva::class);
//        filtramos la lista de reservas por el usuario logado, para eso pasamos usamos la entity Usuario y el método
        $reservas = $repository->findAll();
        return $this->render('gestionCafes/edicionReservas.html.twig', array("reservas" => $reservas));
    }

    /**
     * @Route("/edicionUsuarios", name="edicionUsuarios")
     */
    public function edicionUsuariosAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
//        filtramos la lista de reservas por el usuario logado, para eso pasamos usamos la entity Usuario y el método
//        $usuarios = $repository->findAll();
//        se ordena por el nombre completo en orden ascendente
        $usuarios = $repository->findBy([], ['nombreCompleto' => 'ASC']);

        return $this->render('gestionCafes/edicionUsuarios.html.twig', array("usuarios" => $usuarios));
    }
    /**
     * @Route("/edicionDatosUsuarios/{id}", name="edicionDatosUsuarios")
     */
    public function edicionDatosUsuariosAction(Request $request, $id = null)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
        $usuario = $repository->find($id);

        //Se crea un nuevo formulario a través del método de la clase EdicionUsuarioType
        $formularioUsuario = $this->createForm(EdicionUsuarioType::class, $usuario);
        //A continuación se recoge la información del formlario
        $formularioUsuario->handleRequest($request);
        //si el formulario ha sido enviado y es válida entraría dentro del if
        if ($formularioUsuario->isSubmitted() && $formularioUsuario->isValid() ) {

            $usuario->setUsername($usuario->getEmail());

            $entityManager = $this->getDoctrine()->getManager();

            //$entityManager->persist($usuario);
            $entityManager->flush();

            if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                return $this->redirectToRoute('homepage');
            } else {
                return $this->redirectToRoute('edicionUsuarios');
            }
        }
        return $this->render('gestionCafes/edicionDatosUsuarios.html.twig', array('formularioUsuario' => $formularioUsuario->createView()));
    }

    /**
     * @Route("/borrarUsuario/{id}", name="borrarUsuario")
     */
    public function borrarUsuarioAction(Request $request, $id = null)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
        $usuario = $repository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($usuario);
        $em->flush();
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        } else {
            return $this->redirectToRoute('edicionUsuarios');
        }
    }
}