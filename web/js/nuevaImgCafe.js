var arrayTiposValidos=[
    'image/jpeg',
    'image/png',
    'image/bmp',
    'image/gif'
];
function comprobarTipos(file) {
    //recorremos los tipos válidos definidos mediante array
    for (var i=0;i<arrayTiposValidos.length;i++){
        //si el tipo de archivo seleccionado es alguno de los que tiene el array la validación
        // es correcta y devuelve true
        //alert(file.type);
        if (file.type===arrayTiposValidos[i]){
            return true;
        }
    }
    return false;
}
function onChange(event) {
    // alert("Cambio producido");
    //El input file es un objeto del dom que devuelve un array llamado FileList, y el primer objeto es el
    //elegido por el usuario
    var file=event.target.files[0];
    //console.log(file);
    if (comprobarTipos(file)){
        //alert("Tipo valido");
        var imagenMiniatura=document.getElementById('cafeThumb');
        imagenMiniatura.src=window.URL.createObjectURL(file);

    }else{
        alert ("Tipo de imagen no válida. Elija un tipo válido de entre: jpeg, png, bmp o gif.");
        var imagenMiniatura=document.getElementById('cafeThumb');
        imagenMiniatura.src="";
    }

}