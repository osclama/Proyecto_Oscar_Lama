function cambiarCatSelect() {
    $.getJSON("http://localhost/api/listarCategorias", function (data) {
        var ultimaCategoria = data[data.length - 1];

        console.log(ultimaCategoria);
        //cafe_categorias es id del select al que le vamos a añadir un último elemento
        $(cafe_categorias).append(new Option(ultimaCategoria["nombre"],ultimaCategoria["id"]))
    });
}

function nuevaCatAdd() {
    //nuevaCat es el id que se ha puesto al input en la modificación del formulario de cafés, cafeForm.html.twig
    var ejecutarNuevaCat = $.post("http://localhost/api/insertarCategoria/" + $(nuevaCat).val(), function () {
        $(nuevaCat).val("");
        cambiarCatSelect();
        alert("Nueva Categoría Insertada");
    })
        .fail(function () {
            alert("Se ha producido un error al añadir la nueva categoría")
        });

}