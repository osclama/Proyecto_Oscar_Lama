function controlBorrado(path,reserva) {
    swal({
        title: "Estás seguro?",
        text: "Vas a borrar la reserva con fecha:  "+ reserva,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                // swal("La reserva se ha borrado con éxito", {
                //     icon: "success",
                // });
                window.location.replace(path)
            }
        });
    return false;

}