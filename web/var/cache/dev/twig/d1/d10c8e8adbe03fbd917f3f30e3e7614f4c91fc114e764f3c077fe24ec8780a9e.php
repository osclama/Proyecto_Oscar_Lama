<?php

/* frontal/login.html.twig */
class __TwigTemplate_7768254098eb83a77c9f99bd5ddc5df1f5ea01147d72fc4a136a957abbff7e73 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/login.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo "Login Usuario ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 7
        echo "    ";
        // line 8
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Login de usuario</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Login</li>
        </ol>
        <!-- Intro Content -->

        <div class=\"row\">
            <div class=\"col-lg-6\">
                ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 26, $this->source); })()), "session", array()), "flashbag", array()), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
            // line 27
            echo "                    <p class=\"info\">";
            echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
            echo "</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                ";
        // line 30
        echo "                ";
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 30, $this->source); })())) {
            // line 31
            echo "                    <h3>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 31, $this->source); })()), "messageKey", array()), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 31, $this->source); })()), "messageData", array()), "security"), "html", null, true);
            echo "</h3>
                ";
        }
        // line 33
        echo "                <form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logado");
        echo "\" method=\"post\">
                    ";
        // line 35
        echo "                    <div class=\"control-group form-group\">
                        <div class=\"controls\">
                            <label for=\"username\">Usuario:</label>
                            ";
        // line 39
        echo "                            <input type=\"text\" class=\"form-control\" id=\"username\" name=\"_username\" placeholder=\"email\"
                                   value=\"";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 40, $this->source); })()), "html", null, true);
        echo "\"/>
                        </div>
                    </div>
                    <div class=\"control-group form-group\">
                        <div class=\"controls\">
                            <label for=\"password\">Password:</label>
                            <input type=\"password\" class=\"form-control\" placeholder=\"password\" id=\"password\" name=\"_password\"/>
                        </div>
                    </div>
                    <button type=\"submit\" class=\"btn btn-primary\">Login</button>
                </form>
            </div>
            <div class=\"col-lg-6\">
                <h3>¿Cómo registrarse?</h3>
                <p>Con el registro se podrán hacer reservas para nuestros cursos de café de una forma rápida y
                    sencilla</p>
                <p>Mediante la introducción del correo electrónico y la contraseña se podrá realizar una nueva reserva o
                    anulación de esta</p>
                <p>Si aún no te has registrado en la aplicación, haz click en el enlace de <a
                            href=\"";
        // line 59
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("registro");
        echo "\"><strong>REGISTRO</strong></a></p>
            </div>
        </div>

    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 59,  134 => 40,  131 => 39,  126 => 35,  121 => 33,  115 => 31,  112 => 30,  110 => 29,  101 => 27,  97 => 26,  86 => 18,  74 => 8,  72 => 7,  63 => 6,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{% block titulo %}Login Usuario {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    {#Sacado de la página about de la plantilla#}
    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Login de usuario</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('homepage') }}\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Login</li>
        </ol>
        <!-- Intro Content -->

        <div class=\"row\">
            <div class=\"col-lg-6\">
                {% for mensaje in app.session.flashbag.get('info') %}
                    <p class=\"info\">{{ mensaje }}</p>
                {% endfor %}
                {# Mensaje de error que se muestra si no se pudo realizar el login #}
                {% if error %}
                    <h3>{{ error.messageKey|trans(error.messageData, 'security') }}</h3>
                {% endif %}
                <form action=\"{{ path('logado') }}\" method=\"post\">
                    {#Se añaden los div usados en cafeForm para encerrar los elementos input y darles diseño#}
                    <div class=\"control-group form-group\">
                        <div class=\"controls\">
                            <label for=\"username\">Usuario:</label>
                            {#A su vez se le añade a cada input la clase form-control #}
                            <input type=\"text\" class=\"form-control\" id=\"username\" name=\"_username\" placeholder=\"email\"
                                   value=\"{{ last_username }}\"/>
                        </div>
                    </div>
                    <div class=\"control-group form-group\">
                        <div class=\"controls\">
                            <label for=\"password\">Password:</label>
                            <input type=\"password\" class=\"form-control\" placeholder=\"password\" id=\"password\" name=\"_password\"/>
                        </div>
                    </div>
                    <button type=\"submit\" class=\"btn btn-primary\">Login</button>
                </form>
            </div>
            <div class=\"col-lg-6\">
                <h3>¿Cómo registrarse?</h3>
                <p>Con el registro se podrán hacer reservas para nuestros cursos de café de una forma rápida y
                    sencilla</p>
                <p>Mediante la introducción del correo electrónico y la contraseña se podrá realizar una nueva reserva o
                    anulación de esta</p>
                <p>Si aún no te has registrado en la aplicación, haz click en el enlace de <a
                            href=\"{{ path('registro') }}\"><strong>REGISTRO</strong></a></p>
            </div>
        </div>

    </div>
{% endblock %}", "frontal/login.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\login.html.twig");
    }
}
