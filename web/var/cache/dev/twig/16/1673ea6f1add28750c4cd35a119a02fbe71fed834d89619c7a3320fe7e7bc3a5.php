<?php

/* frontal/cafes.html.twig */
class __TwigTemplate_8c7372541f150c1e5841ce8c5e9824795b49fba7cc42a92b43fd308db324b346 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/cafes.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/cafes.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/cafes.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo "Cafés ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 7
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Datos del Café
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias");
        echo "\">Categorías</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("todosNuestrosCafes");
        echo "\">Cafes</a>
            </li>
            <li class=\"breadcrumb-item active\">Datos del Café</li>
        </ol>

        <!-- Portfolio Item Row -->
        <div class=\"row\">

            <div class=\"col-md-12 text-center\">
                ";
        // line 33
        echo "                <img class=\"img-fluid\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(((isset($context["fotosCafes"]) || array_key_exists("fotosCafes", $context) ? $context["fotosCafes"] : (function () { throw new Twig_Error_Runtime('Variable "fotosCafes" does not exist.', 33, $this->source); })()) . twig_get_attribute($this->env, $this->source, (isset($context["datosCafe"]) || array_key_exists("datosCafe", $context) ? $context["datosCafe"] : (function () { throw new Twig_Error_Runtime('Variable "datosCafe" does not exist.', 33, $this->source); })()), "foto", array()))), "html", null, true);
        echo "\" alt=\"\">
            </div>

            <div class=\"col-md-12\">
                <h2 class=\"my-4 text-primary\">";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["datosCafe"]) || array_key_exists("datosCafe", $context) ? $context["datosCafe"] : (function () { throw new Twig_Error_Runtime('Variable "datosCafe" does not exist.', 37, $this->source); })()), "nombre", array()), "html", null, true);
        echo "</h2>


                <div>
                        <h4 class=\"my-3\">Categorías del café                        </h4>

                    ";
        // line 44
        echo "                            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["datosCafe"]) || array_key_exists("datosCafe", $context) ? $context["datosCafe"] : (function () { throw new Twig_Error_Runtime('Variable "datosCafe" does not exist.', 44, $this->source); })()), "categorias", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            // line 45
            echo "                                ";
            // line 46
            echo "
                                <a class=\"btn btn-primary m-1\"
                                   href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("categoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "id", array()), "nombreCategoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()), "html", null, true);
            echo "</a>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                </div>

                <h4 class=\"my-3\">Descripción</h4>

                <p>";
        // line 54
        echo twig_get_attribute($this->env, $this->source, (isset($context["datosCafe"]) || array_key_exists("datosCafe", $context) ? $context["datosCafe"] : (function () { throw new Twig_Error_Runtime('Variable "datosCafe" does not exist.', 54, $this->source); })()), "descripcion", array());
        echo "</p>
                <h4 class=\"my-3\">Características</h4>
                <p>";
        // line 56
        echo twig_get_attribute($this->env, $this->source, (isset($context["datosCafe"]) || array_key_exists("datosCafe", $context) ? $context["datosCafe"] : (function () { throw new Twig_Error_Runtime('Variable "datosCafe" does not exist.', 56, $this->source); })()), "caracteristicas", array());
        echo "</p>
            </div>
            <div class=\"float-none\"></div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/cafes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 56,  151 => 54,  145 => 50,  135 => 48,  131 => 46,  129 => 45,  124 => 44,  115 => 37,  107 => 33,  95 => 22,  89 => 19,  83 => 16,  72 => 7,  63 => 6,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{% block titulo %}Cafés {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Datos del Café
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('homepage') }}\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('listadoCategorias') }}\">Categorías</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('todosNuestrosCafes') }}\">Cafes</a>
            </li>
            <li class=\"breadcrumb-item active\">Datos del Café</li>
        </ol>

        <!-- Portfolio Item Row -->
        <div class=\"row\">

            <div class=\"col-md-12 text-center\">
                {#fotosCafes es el parámetro global definido en el config.yml con la ruta a la carpeta img/fotosCafes
                a esto se le concatena los datos en string guardados en la ddbb del nombre de la foto en md5#}
                <img class=\"img-fluid\" src=\"{{ asset(fotosCafes~datosCafe.foto) }}\" alt=\"\">
            </div>

            <div class=\"col-md-12\">
                <h2 class=\"my-4 text-primary\">{{ datosCafe.nombre }}</h2>


                <div>
                        <h4 class=\"my-3\">Categorías del café                        </h4>

                    {#{{ dump(datosCafe) }}#}
                            {% for categoria in datosCafe.categorias %}
                                {#{{ dump(categoria) }}#}

                                <a class=\"btn btn-primary m-1\"
                                   href=\"{{ path('listadoCafes',{'categoria':categoria.id, 'nombreCategoria':categoria.nombre}) }}\">{{ categoria.nombre }}</a>
                            {% endfor %}
                </div>

                <h4 class=\"my-3\">Descripción</h4>

                <p>{{ datosCafe.descripcion|raw }}</p>
                <h4 class=\"my-3\">Características</h4>
                <p>{{ datosCafe.caracteristicas|raw }}</p>
            </div>
            <div class=\"float-none\"></div>
        </div>
    </div>
{% endblock %}", "frontal/cafes.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\cafes.html.twig");
    }
}
