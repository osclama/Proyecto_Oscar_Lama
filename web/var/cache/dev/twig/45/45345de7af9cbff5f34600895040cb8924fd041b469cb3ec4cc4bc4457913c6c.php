<?php

/* gestionCafes/edicionReservas.html.twig */
class __TwigTemplate_d42ae652231a6b06782bee6280ba3d33697dcadf84321ede357e484dc4f1a420 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "gestionCafes/edicionReservas.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gestionCafes/edicionReservas.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gestionCafes/edicionReservas.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo "Listado de Reservas ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 8
        echo "    ";
        // line 9
        echo "    <div class=\"container\">
        ";
        // line 11
        echo "        <h1 class=\"mt-4 mb-3\">Listado de Reservas</h1>
        ";
        // line 13
        echo "        <div class=\"row\">
            <div class=\"col-md-2\">
            </div>
            <div class=\"col-md-8\">
                <table class=\"table\">
                    <thead class=\"thead-dark\">
                    <tr style=\"text-align: center\">
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">Fecha</th>
                        <th scope=\"col\">Num Asistentes</th>
                        <th scope=\"col\">Usuario</th>
                        <th scope=\"col\">Teléfono</th>
                        <th scope=\"col\">Editar</th>
                        <th scope=\"col\">Borrar</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 30
        $context["contador"] = 0;
        // line 31
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reservas"]) || array_key_exists("reservas", $context) ? $context["reservas"] : (function () { throw new Twig_Error_Runtime('Variable "reservas" does not exist.', 31, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["reserva"]) {
            // line 32
            echo "                        <tr style=\"text-align: center\">
                            ";
            // line 33
            $context["contador"] = ((isset($context["contador"]) || array_key_exists("contador", $context) ? $context["contador"] : (function () { throw new Twig_Error_Runtime('Variable "contador" does not exist.', 33, $this->source); })()) + 1);
            // line 34
            echo "                            <th scope=\"row\">";
            echo twig_escape_filter($this->env, (isset($context["contador"]) || array_key_exists("contador", $context) ? $context["contador"] : (function () { throw new Twig_Error_Runtime('Variable "contador" does not exist.', 34, $this->source); })()), "html", null, true);
            echo "</th>
                            ";
            // line 36
            echo "                            <td>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reserva"], "fecha", array()), "Y/m/d h:i"), "html", null, true);
            echo "</td>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reserva"], "asistentes", array()), "html", null, true);
            echo "</td>
                            ";
            // line 39
            echo "                            ";
            // line 40
            echo "                            <td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reserva"], "usuario", array()), "nombreCompleto", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reserva"], "usuario", array()), "telefono", array()), "html", null, true);
            echo "</td>

                            ";
            // line 44
            echo "                            <td><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevaReserva", array("id" => twig_get_attribute($this->env, $this->source, $context["reserva"], "id", array()), "usuario" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reserva"], "usuario", array()), "id", array()))), "html", null, true);
            echo "\"><i class=\"fas fa-edit\"
                                                                                            style=\"font-size: 130%\"></i></a>
                            </td>
                            ";
            // line 48
            echo "                            <td><a href=\"#\"
                                   onClick=\"return controlBorrado('";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("borrarReserva", array("id" => twig_get_attribute($this->env, $this->source, $context["reserva"], "id", array()))), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reserva"], "fecha", array()), "Y/m/d h:i"), "html", null, true);
            echo "')\">
                                    <i class=\"far fa-trash-alt\"
                                       style=\"font-size: 140%\"></i></a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reserva'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    </tbody>
                </table>
            </div>
            <div class=\"col-md-2\">
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 63
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 64
        echo "    <script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>
    <script src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/controlBorrado.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "gestionCafes/edicionReservas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 65,  188 => 64,  179 => 63,  162 => 55,  148 => 49,  145 => 48,  138 => 44,  133 => 41,  128 => 40,  126 => 39,  122 => 37,  117 => 36,  112 => 34,  110 => 33,  107 => 32,  102 => 31,  100 => 30,  81 => 13,  78 => 11,  75 => 9,  73 => 8,  64 => 7,  46 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{#TITULO#}
{% block titulo %}Listado de Reservas {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    {#Contenedor para poder darle estilos al formulario#}
    <div class=\"container\">
        {#Título de la página#}
        <h1 class=\"mt-4 mb-3\">Listado de Reservas</h1>
        {#Para aislarlo del título se envuelve el formulario en un class row#}
        <div class=\"row\">
            <div class=\"col-md-2\">
            </div>
            <div class=\"col-md-8\">
                <table class=\"table\">
                    <thead class=\"thead-dark\">
                    <tr style=\"text-align: center\">
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">Fecha</th>
                        <th scope=\"col\">Num Asistentes</th>
                        <th scope=\"col\">Usuario</th>
                        <th scope=\"col\">Teléfono</th>
                        <th scope=\"col\">Editar</th>
                        <th scope=\"col\">Borrar</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% set contador=0 %}
                    {% for reserva in reservas %}
                        <tr style=\"text-align: center\">
                            {% set contador=contador+1 %}
                            <th scope=\"row\">{{ contador }}</th>
                            {#El campo de la fecha lo filtraremos con date para que muestre el formato deseado#}
                            <td>{{ reserva.fecha|date('Y/m/d h:i') }}</td>
                            <td>{{ reserva.asistentes }}</td>
                            {#{{ dump(reserva) }}#}
                            {#{{ dump(reserva.usuario) }}#}
                            <td>{{ reserva.usuario.nombreCompleto }}</td>
                            <td>{{ reserva.usuario.telefono }}</td>

                            {#para que redireccione con el id la variable enviada se tiene que llamar id, no vale otro nombre#}
                            <td><a href=\"{{ path('nuevaReserva',{'id': reserva.id,'usuario': reserva.usuario.id }) }}\"><i class=\"fas fa-edit\"
                                                                                            style=\"font-size: 130%\"></i></a>
                            </td>
                            {#Al asignarle al onclick el return se ejecuta la función y devuelve lo que devuelva esta #}
                            <td><a href=\"#\"
                                   onClick=\"return controlBorrado('{{ path('borrarReserva',{'id': reserva.id }) }}','{{ reserva.fecha|date('Y/m/d h:i') }}')\">
                                    <i class=\"far fa-trash-alt\"
                                       style=\"font-size: 140%\"></i></a>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
            <div class=\"col-md-2\">
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    <script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>
    <script src=\"{{ asset('js/controlBorrado.js') }}\"></script>
{% endblock %}

", "gestionCafes/edicionReservas.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\gestionCafes\\edicionReservas.html.twig");
    }
}
