<?php

/* frontal/index.html.twig */
class __TwigTemplate_77a23917f1be42a4c5446c72ce8bf9bd3893722a630ff5aa95a9ec7ef4258c52 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/index.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo " Cafés Mauro ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 7
        echo "    <header>
        <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
            <ol class=\"carousel-indicators\">
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>
            </ol>
            <div class=\"carousel-inner\" role=\"listbox\">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class=\"carousel-item active\" style=\"background-image: url(";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/slide01.jpg"), "html", null, true);
        echo ")\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>La calidad nuestra razón de ser</p>
                    </div>
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class=\"carousel-item\" style=\"background-image: url(";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/slide02.jpg"), "html", null, true);
        echo ")\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>Desde 1950</p>
                    </div>
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class=\"carousel-item\" style=\"background-image: url(";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/slide03.jpg"), "html", null, true);
        echo ")\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>Especialistas en Café</p>
                    </div>
                </div>
            </div>
            <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
                <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
                <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
        </div>
    </header>
    <div class=\"container\">
        <!-- Portfolio Section -->
        <h1 class=\"my-4\">Nuestras Recomentaciones</h1>
        ";
        // line 51
        echo "        <div class=\"row\">
            ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cafes"]) || array_key_exists("cafes", $context) ? $context["cafes"] : (function () { throw new Twig_Error_Runtime('Variable "cafes" does not exist.', 52, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["cafe"]) {
            // line 53
            echo "                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        ";
            // line 57
            echo "                        ";
            // line 59
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\"><img class=\"card-img-top\"
                                                                          src=";
            // line 60
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(((isset($context["fotosCafes"]) || array_key_exists("fotosCafes", $context) ? $context["fotosCafes"] : (function () { throw new Twig_Error_Runtime('Variable "fotosCafes" does not exist.', 60, $this->source); })()) . twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 61
            echo "                                                                          alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                ";
            // line 65
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "nombre", array()), "html", null, true);
            echo "</a>
                            </h4>
                            <p class=\"card-text\">";
            // line 67
            echo twig_get_attribute($this->env, $this->source, $context["cafe"], "caracteristicas", array());
            echo "</p>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cafe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "        </div>
        <!-- /.container -->

        <!-- Paginacion -->
        ";
        // line 77
        echo "        ";
        if (((isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 77, $this->source); })()) > 1)) {
            // line 78
            echo "            <ul class=\"pagination justify-content-center\">
                ";
            // line 80
            echo "                ";
            if (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 80, $this->source); })()) > 1)) {
                // line 81
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\" href=\"";
                // line 82
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => (((((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 82, $this->source); })()) - 1) < 1)) ? (1) : (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 82, $this->source); })()) - 1))))), "html", null, true);
                echo "\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                ";
            }
            // line 89
            echo "                ";
            // line 90
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 90, $this->source); })())));
            foreach ($context['_seq'] as $context["_key"] => $context["indice"]) {
                // line 91
                echo "                    ";
                if (($context["indice"] == (isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 91, $this->source); })()))) {
                    // line 92
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"";
                    // line 94
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                } else {
                    // line 97
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"";
                    // line 98
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                }
                // line 101
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 102
            echo "                ";
            // line 103
            echo "                ";
            if (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 103, $this->source); })()) < (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 103, $this->source); })()))) {
                // line 104
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 106
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage", array("pagina" => (((((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 106, $this->source); })()) + 1) <= (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 106, $this->source); })()))) ? (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 106, $this->source); })()) + 1)) : ((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 106, $this->source); })()))))), "html", null, true);
                echo "\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                ";
            }
            // line 113
            echo "            </ul>
        ";
        }
        // line 115
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 115,  260 => 113,  250 => 106,  246 => 104,  243 => 103,  241 => 102,  235 => 101,  227 => 98,  224 => 97,  216 => 94,  212 => 92,  209 => 91,  204 => 90,  202 => 89,  192 => 82,  189 => 81,  186 => 80,  183 => 78,  180 => 77,  174 => 72,  163 => 67,  155 => 65,  150 => 61,  144 => 60,  139 => 59,  137 => 57,  133 => 53,  129 => 52,  126 => 51,  103 => 30,  93 => 23,  83 => 16,  72 => 7,  63 => 6,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{% block titulo %} Cafés Mauro {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    <header>
        <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
            <ol class=\"carousel-indicators\">
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>
                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>
            </ol>
            <div class=\"carousel-inner\" role=\"listbox\">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class=\"carousel-item active\" style=\"background-image: url({{ asset('img/slide01.jpg') }})\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>La calidad nuestra razón de ser</p>
                    </div>
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class=\"carousel-item\" style=\"background-image: url({{ asset('img/slide02.jpg') }})\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>Desde 1950</p>
                    </div>
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class=\"carousel-item\" style=\"background-image: url({{ asset('img/slide03.jpg') }})\">
                    <div class=\"carousel-caption d-none d-md-block\">
                        <h3>Cafés Mauro</h3>
                        <p>Especialistas en Café</p>
                    </div>
                </div>
            </div>
            <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
                <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
                <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
        </div>
    </header>
    <div class=\"container\">
        <!-- Portfolio Section -->
        <h1 class=\"my-4\">Nuestras Recomentaciones</h1>
        {#Recorremos los datosCafes, que hemos enviado a la plantilla mediante el array llamado cafes, mediante un bucle for#}
        <div class=\"row\">
            {% for cafe in cafes %}
                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        {#El href va a redireccionar al path definido con name en el DefaultController, y se le pasa como parámetro
                        el id del café seleccionado para que redirecione a la plantilla cafes.html.twig con ese parámetro y muestre sus datos#}
                        {#fotosCafes es el parámetro global definido en el config.yml con la ruta a la carpeta img/fotosCafes
               a esto se le concatena los datos en string guardados en la ddbb del nombre de la foto en md5#}
                        <a href=\"{{ path('cafes',{'id':cafe.id}) }}\"><img class=\"card-img-top\"
                                                                          src={% if  cafe.foto|length>0 %}{{ asset(fotosCafes~cafe.foto) }}{% else %}\"http://placehold.it/700x400\"{% endif %}
                                                                          alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                {#Va a redireccionar tanto el nombre del café como su imagen#}
                                <a href=\"{{ path('cafes',{'id':cafe.id}) }}\">{{ cafe.nombre }}</a>
                            </h4>
                            <p class=\"card-text\">{{ cafe.caracteristicas|raw }}</p>
                        </div>
                    </div>
                </div>
            {% endfor %}
        </div>
        <!-- /.container -->

        <!-- Paginacion -->
        {#Si el número total de páginas es mayor a 1 se mostrará la paginación#}
        {% if numTotalPaginas>1 %}
            <ul class=\"pagination justify-content-center\">
                {#Botón de Anterior#}
                {% if  paginaActual>1 %}
                    <li class=\"page-item\">
                        <a class=\"page-link\" href=\"{{ path ('homepage', {pagina:paginaActual-1<1?1:paginaActual-1} ) }}\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                {% endif %}
                {#Bucle que rellena el número de items con números de páginas de cafés top que hay, cada página con 3 elementos#}
                {% for indice in 1..numTotalPaginas %}
                    {% if indice==paginaActual %}
                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"{{ path ('homepage', {pagina:indice} ) }}\">{{ indice }}</a>
                        </li>
                    {% else %}
                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"{{ path ('homepage', {pagina:indice} ) }}\">{{ indice }}</a>
                        </li>
                    {% endif %}
                {% endfor %}
                {#Boton Siguiente#}
                {% if  paginaActual<numTotalPaginas %}
                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"{{ path ('homepage', {pagina:paginaActual+1<=numTotalPaginas?paginaActual+1:paginaActual} ) }}\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                {% endif %}
            </ul>
        {% endif %}

    </div>
{% endblock %}", "frontal/index.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\index.html.twig");
    }
}
