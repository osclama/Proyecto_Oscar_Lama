<?php

/* frontal/listadoCafes.html.twig */
class __TwigTemplate_1e7e5d2a043940380c615e5b2ec8d460f2304664ef98827870adc3a8a9869780 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/listadoCafes.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/listadoCafes.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/listadoCafes.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo " Cafés Mauro ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 7
        echo "    <header>

    </header>
    <div class=\"container\">
        <!-- Portfolio Section -->
        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["nombreCategoria"]) || array_key_exists("nombreCategoria", $context) ? $context["nombreCategoria"] : (function () { throw new Twig_Error_Runtime('Variable "nombreCategoria" does not exist.', 14, $this->source); })()), "html", null, true);
        echo "</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias");
        echo "\">Categorías</a>
            </li>
            <li class=\"breadcrumb-item active\">Cafés</li>
        </ol>
        ";
        // line 27
        echo "        <div class=\"row\">
            ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["datosCafes"]) || array_key_exists("datosCafes", $context) ? $context["datosCafes"] : (function () { throw new Twig_Error_Runtime('Variable "datosCafes" does not exist.', 28, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["cafe"]) {
            // line 29
            echo "                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        ";
            // line 33
            echo "                        ";
            // line 35
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\"><img class=\"card-img-top\"
                                                                          src=";
            // line 36
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(((isset($context["fotosCafes"]) || array_key_exists("fotosCafes", $context) ? $context["fotosCafes"] : (function () { throw new Twig_Error_Runtime('Variable "fotosCafes" does not exist.', 36, $this->source); })()) . twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 37
            echo "                                                                          alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                ";
            // line 41
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "nombre", array()), "html", null, true);
            echo "</a>
                            </h4>
                            <p class=\"card-text\">";
            // line 43
            echo twig_get_attribute($this->env, $this->source, $context["cafe"], "caracteristicas", array());
            echo "</p>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cafe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "        </div>
        <!-- /.container -->

        <!-- Paginacion -->
        ";
        // line 53
        echo "        ";
        // line 54
        echo "        ";
        if (((isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 54, $this->source); })()) > 1)) {
            // line 55
            echo "            <ul class=\"pagination justify-content-center\">
                ";
            // line 57
            echo "                ";
            if (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 57, $this->source); })()) > 1)) {
                // line 58
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\" href=\"";
                // line 59
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("pagina" => (((((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 59, $this->source); })()) - 1) < 1)) ? (1) : (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 59, $this->source); })()) - 1))), "categoria" => (isset($context["categoria"]) || array_key_exists("categoria", $context) ? $context["categoria"] : (function () { throw new Twig_Error_Runtime('Variable "categoria" does not exist.', 59, $this->source); })()))), "html", null, true);
                echo "\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                ";
            }
            // line 66
            echo "                ";
            // line 67
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 67, $this->source); })())));
            foreach ($context['_seq'] as $context["_key"] => $context["indice"]) {
                // line 68
                echo "                    ";
                if (($context["indice"] == (isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 68, $this->source); })()))) {
                    // line 69
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("pagina" => $context["indice"], "categoria" => (isset($context["categoria"]) || array_key_exists("categoria", $context) ? $context["categoria"] : (function () { throw new Twig_Error_Runtime('Variable "categoria" does not exist.', 71, $this->source); })()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                } else {
                    // line 74
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"";
                    // line 75
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("pagina" => $context["indice"], "categoria" => (isset($context["categoria"]) || array_key_exists("categoria", $context) ? $context["categoria"] : (function () { throw new Twig_Error_Runtime('Variable "categoria" does not exist.', 75, $this->source); })()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                }
                // line 78
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                ";
            // line 80
            echo "                ";
            if (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 80, $this->source); })()) < (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 80, $this->source); })()))) {
                // line 81
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 83
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("pagina" => (((((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 83, $this->source); })()) + 1) <= (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 83, $this->source); })()))) ? (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 83, $this->source); })()) + 1)) : ((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 83, $this->source); })()))), "categoria" => (isset($context["categoria"]) || array_key_exists("categoria", $context) ? $context["categoria"] : (function () { throw new Twig_Error_Runtime('Variable "categoria" does not exist.', 83, $this->source); })()))), "html", null, true);
                echo "\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                ";
            }
            // line 90
            echo "            </ul>
        ";
        }
        // line 92
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/listadoCafes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 92,  238 => 90,  228 => 83,  224 => 81,  221 => 80,  219 => 79,  213 => 78,  205 => 75,  202 => 74,  194 => 71,  190 => 69,  187 => 68,  182 => 67,  180 => 66,  170 => 59,  167 => 58,  164 => 57,  161 => 55,  158 => 54,  156 => 53,  150 => 48,  139 => 43,  131 => 41,  126 => 37,  120 => 36,  115 => 35,  113 => 33,  109 => 29,  105 => 28,  102 => 27,  95 => 22,  89 => 19,  81 => 14,  72 => 7,  63 => 6,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{% block titulo %} Cafés Mauro {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    <header>

    </header>
    <div class=\"container\">
        <!-- Portfolio Section -->
        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>{{ nombreCategoria }}</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('homepage') }}\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('listadoCategorias') }}\">Categorías</a>
            </li>
            <li class=\"breadcrumb-item active\">Cafés</li>
        </ol>
        {#Recorremos los datosCafes, que hemos enviado a la plantilla mediante el array llamado cafes, mediante un bucle for#}
        <div class=\"row\">
            {% for cafe in datosCafes %}
                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        {#El href va a redireccionar al path definido con name en el DefaultController, y se le pasa como parámetro
                        el id del café seleccionado para que redirecione a la plantilla cafes.html.twig con ese parámetro y muestre sus datos#}
                        {#fotosCafes es el parámetro global definido en el config.yml con la ruta a la carpeta img/fotosCafes
               a esto se le concatena los datos en string guardados en la ddbb del nombre de la foto en md5#}
                        <a href=\"{{ path('cafes',{'id':cafe.id}) }}\"><img class=\"card-img-top\"
                                                                          src={% if  cafe.foto|length>0 %}{{ asset(fotosCafes~cafe.foto) }}{% else %}\"http://placehold.it/700x400\"{% endif %}
                                                                          alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                {#Va a redireccionar tanto el nombre del café como su imagen#}
                                <a href=\"{{ path('cafes',{'id':cafe.id}) }}\">{{ cafe.nombre }}</a>
                            </h4>
                            <p class=\"card-text\">{{ cafe.caracteristicas|raw }}</p>
                        </div>
                    </div>
                </div>
            {% endfor %}
        </div>
        <!-- /.container -->

        <!-- Paginacion -->
        {#La paginación se mostrará si el número de páginas es mayor que 1#}
        {#{{ dump(numTotalPaginas) }}#}
        {% if  numTotalPaginas>1 %}
            <ul class=\"pagination justify-content-center\">
                {#Botón de Anterior#}
                {% if  paginaActual>1 %}
                    <li class=\"page-item\">
                        <a class=\"page-link\" href=\"{{ path ('listadoCafes', {pagina:paginaActual-1<1?1:paginaActual-1, categoria:categoria} ) }}\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                {% endif %}
                {#Bucle que rellena el número de items con números de páginas de cafés top que hay, cada página con 3 elementos#}
                {% for indice in 1..numTotalPaginas %}
                    {% if indice==paginaActual %}
                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"{{ path ('listadoCafes', {pagina:indice, categoria:categoria} ) }}\">{{ indice }}</a>
                        </li>
                    {% else %}
                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"{{ path ('listadoCafes', {pagina:indice, categoria:categoria} ) }}\">{{ indice }}</a>
                        </li>
                    {% endif %}
                {% endfor %}
                {#Boton Siguiente#}
                {% if  paginaActual<numTotalPaginas %}
                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"{{ path ('listadoCafes', {pagina:paginaActual+1<=numTotalPaginas?paginaActual+1:paginaActual, categoria:categoria} ) }}\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                {% endif %}
            </ul>
        {% endif %}

    </div>
{% endblock %}", "frontal/listadoCafes.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\listadoCafes.html.twig");
    }
}
