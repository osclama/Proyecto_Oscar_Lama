<?php

/* frontal/listadoCategorias.html.twig */
class __TwigTemplate_0385fc0e916b988e8258fb474a10e398053dedd28740af52228ab114f7042268 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/listadoCategorias.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/listadoCategorias.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontal/listadoCategorias.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo " Cafés Mauro ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 7
        echo "    <header>

    </header>
    <div class=\"container\">
        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Categorías</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Categorías</li>
        </ol>
        <!-- Image Header -->
        <!-- Portfolio Section -->
        ";
        // line 25
        echo "        <div class=\"row\">
            ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["datosCategorias"]) || array_key_exists("datosCategorias", $context) ? $context["datosCategorias"] : (function () { throw new Twig_Error_Runtime('Variable "datosCategorias" does not exist.', 26, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            // line 27
            echo "                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        ";
            // line 31
            echo "                        ";
            // line 33
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("categoria", array("id" => twig_get_attribute($this->env, $this->source, $context["categoria"], "id", array()))), "html", null, true);
            echo "\"><img class=\"card-img-top\"
                                                                                   src=";
            // line 34
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categoria"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(((isset($context["fotosCategorias"]) || array_key_exists("fotosCategorias", $context) ? $context["fotosCategorias"] : (function () { throw new Twig_Error_Runtime('Variable "fotosCategorias" does not exist.', 34, $this->source); })()) . twig_get_attribute($this->env, $this->source, $context["categoria"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 35
            echo "                                                                                   alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                ";
            // line 39
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("categoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "id", array()), "nombreCategoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()), "html", null, true);
            echo "</a>
                            </h4>
                            <p class=\"card-text\">";
            // line 41
            echo twig_get_attribute($this->env, $this->source, $context["categoria"], "descripcion", array());
            echo "</p>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </div>
        <!-- /.container -->
        ";
        // line 49
        echo "
        <!-- Paginacion -->
        ";
        // line 52
        echo "        ";
        if (((isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 52, $this->source); })()) > 1)) {
            // line 53
            echo "            <ul class=\"pagination justify-content-center\">
                ";
            // line 55
            echo "                ";
            if (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 55, $this->source); })()) > 1)) {
                // line 56
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 58
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => (((((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 58, $this->source); })()) - 1) < 1)) ? (1) : (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 58, $this->source); })()) - 1))))), "html", null, true);
                echo "\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                ";
            }
            // line 65
            echo "                ";
            // line 66
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 66, $this->source); })())));
            foreach ($context['_seq'] as $context["_key"] => $context["indice"]) {
                // line 67
                echo "
                    ";
                // line 68
                if (($context["indice"] == (isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 68, $this->source); })()))) {
                    // line 69
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                } else {
                    // line 74
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link\"
                               href=\"";
                    // line 76
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                }
                // line 79
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 80
            echo "                ";
            // line 81
            echo "                ";
            if (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 81, $this->source); })()) < (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 81, $this->source); })()))) {
                // line 82
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 84
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => (((((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 84, $this->source); })()) + 1) <= (isset($context["numTotalPaginas"]) || array_key_exists("numTotalPaginas", $context) ? $context["numTotalPaginas"] : (function () { throw new Twig_Error_Runtime('Variable "numTotalPaginas" does not exist.', 84, $this->source); })()))) ? (((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 84, $this->source); })()) + 1)) : ((isset($context["paginaActual"]) || array_key_exists("paginaActual", $context) ? $context["paginaActual"] : (function () { throw new Twig_Error_Runtime('Variable "paginaActual" does not exist.', 84, $this->source); })()))))), "html", null, true);
                echo "\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                ";
            }
            // line 91
            echo "            </ul>
        ";
        }
        // line 93
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "frontal/listadoCategorias.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 93,  234 => 91,  224 => 84,  220 => 82,  217 => 81,  215 => 80,  209 => 79,  201 => 76,  197 => 74,  189 => 71,  185 => 69,  183 => 68,  180 => 67,  175 => 66,  173 => 65,  163 => 58,  159 => 56,  156 => 55,  153 => 53,  150 => 52,  146 => 49,  142 => 46,  131 => 41,  123 => 39,  118 => 35,  112 => 34,  107 => 33,  105 => 31,  101 => 27,  97 => 26,  94 => 25,  85 => 18,  72 => 7,  63 => 6,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{% block titulo %} Cafés Mauro {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    <header>

    </header>
    <div class=\"container\">
        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Categorías</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"{{ path('homepage') }}\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Categorías</li>
        </ol>
        <!-- Image Header -->
        <!-- Portfolio Section -->
        {#Recorremos los datosCafes, que hemos enviado a la plantilla mediante el array llamado cafes, mediante un bucle for#}
        <div class=\"row\">
            {% for categoria in datosCategorias %}
                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        {#El href va a redireccionar al path definido con name en el DefaultController, y se le pasa como parámetro
                        el id del café seleccionado para que redirecione a la plantilla cafes.html.twig con ese parámetro y muestre sus datos#}
                        {#fotosCafes es el parámetro global definido en el config.yml con la ruta a la carpeta img/fotosCafes
               a esto se le concatena los datos en string guardados en la ddbb del nombre de la foto en md5#}
                        <a href=\"{{ path('categoria',{'id':categoria.id}) }}\"><img class=\"card-img-top\"
                                                                                   src={% if  categoria.foto|length>0 %}{{ asset(fotosCategorias~categoria.foto) }}{% else %}\"http://placehold.it/700x400\"{% endif %}
                                                                                   alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                {#Va a redireccionar tanto el nombre del café como su imagen#}
                                <a href=\"{{ path('listadoCafes',{'categoria':categoria.id, 'nombreCategoria':categoria.nombre}) }}\">{{ categoria.nombre }}</a>
                            </h4>
                            <p class=\"card-text\">{{ categoria.descripcion|raw }}</p>
                        </div>
                    </div>
                </div>
            {% endfor %}
        </div>
        <!-- /.container -->
        {#{{ dump(numTotalPaginas) }}#}

        <!-- Paginacion -->
        {#Si el número de páginas no es mayor a 1 no se mostrará la paginación #}
        {% if numTotalPaginas>1 %}
            <ul class=\"pagination justify-content-center\">
                {#Botón de Anterior#}
                {% if  paginaActual>1 %}
                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"{{ path ('listadoCategorias', {pagina:paginaActual-1<1?1:paginaActual-1} ) }}\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                {% endif %}
                {#Bucle que rellena el número de items con números de páginas de categorías que hay, cada página con 6 elementos#}
                {% for indice in 1..numTotalPaginas %}

                    {% if indice==paginaActual %}
                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"{{ path ('listadoCategorias', {pagina:indice} ) }}\">{{ indice }}</a>
                        </li>
                    {% else %}
                        <li class=\"page-item\">
                            <a class=\"page-link\"
                               href=\"{{ path ('listadoCategorias', {pagina:indice} ) }}\">{{ indice }}</a>
                        </li>
                    {% endif %}
                {% endfor %}
                {#Boton Siguiente#}
                {% if  paginaActual<numTotalPaginas %}
                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"{{ path ('listadoCategorias', {pagina:paginaActual+1<=numTotalPaginas?paginaActual+1:paginaActual} ) }}\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                {% endif %}
            </ul>
        {% endif %}

    </div>
{% endblock %}", "frontal/listadoCategorias.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\listadoCategorias.html.twig");
    }
}
