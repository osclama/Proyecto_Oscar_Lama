<?php

/* gestionCafes/edicionCafes.html.twig */
class __TwigTemplate_a3c724747bf258cf18a481d7b1610a0366837691c0a49b809d038904a7df1c90 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "gestionCafes/edicionCafes.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gestionCafes/edicionCafes.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gestionCafes/edicionCafes.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_titulo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "titulo"));

        echo "Listado de Categorías ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 8
        echo "    ";
        // line 9
        echo "    <div class=\"container\">
        ";
        // line 11
        echo "        <h2 class=\"mt-4 mb-3\">Listado de Cafés</h2>
        ";
        // line 13
        echo "        <div class=\"row\">
            <div class=\"col-md-2\">
            </div>
            <div class=\"col-md-8\">
                <table class=\"table\">
                    <thead class=\"thead-dark\">
                    <tr style=\"text-align: center\">
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">Foto</th>
                        <th scope=\"col\">Nombre</th>
                        <th scope=\"col\">Editar</th>
                        <th scope=\"col\">Borrar</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 28
        $context["contador"] = 0;
        // line 29
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cafes"]) || array_key_exists("cafes", $context) ? $context["cafes"] : (function () { throw new Twig_Error_Runtime('Variable "cafes" does not exist.', 29, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["cafe"]) {
            // line 30
            echo "                        <tr style=\"text-align: center\">
                            ";
            // line 31
            $context["contador"] = ((isset($context["contador"]) || array_key_exists("contador", $context) ? $context["contador"] : (function () { throw new Twig_Error_Runtime('Variable "contador" does not exist.', 31, $this->source); })()) + 1);
            // line 32
            echo "                            <th scope=\"row\">";
            echo twig_escape_filter($this->env, (isset($context["contador"]) || array_key_exists("contador", $context) ? $context["contador"] : (function () { throw new Twig_Error_Runtime('Variable "contador" does not exist.', 32, $this->source); })()), "html", null, true);
            echo "</th>
                            ";
            // line 34
            echo "                            <td>
                                <img src=";
            // line 35
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(((isset($context["fotosCafes"]) || array_key_exists("fotosCafes", $context) ? $context["fotosCafes"] : (function () { throw new Twig_Error_Runtime('Variable "fotosCafes" does not exist.', 35, $this->source); })()) . twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 36
            echo "                                     width=\"200\" alt=\"\">
                            </td>
                            <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "nombre", array()), "html", null, true);
            echo "</td>
                            ";
            // line 40
            echo "                            ";
            // line 41
            echo "                            <td><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevoCafe", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\">
                                    <i class=\"fas fa-edit\"
                                       style=\"font-size: 130%\"></i>
                                </a>
                            </td>
                            ";
            // line 47
            echo "                            <td><a href=\"#\"
                                   onClick=\"return controlBorrado('";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("borrarCafe", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "')\">
                                    <i class=\"far fa-trash-alt\"
                                       style=\"font-size: 140%\"></i>
                                </a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cafe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    </tbody>
                </table>
            </div>
            <div class=\"col-md-2\">
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 63
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 64
        echo "    <script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>
    <script src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/controlBorrado.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "gestionCafes/edicionCafes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 65,  185 => 64,  176 => 63,  159 => 55,  146 => 48,  143 => 47,  134 => 41,  132 => 40,  128 => 38,  124 => 36,  118 => 35,  115 => 34,  110 => 32,  108 => 31,  105 => 30,  100 => 29,  98 => 28,  81 => 13,  78 => 11,  75 => 9,  73 => 8,  64 => 7,  46 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Plantilla de la aplicación #}
{% extends 'base.html.twig' %}
{#TITULO#}
{% block titulo %}Listado de Categorías {% endblock %}

{# CONTENIDO #}
{% block contenido %}
    {#Contenedor para poder darle estilos al formulario#}
    <div class=\"container\">
        {#Título de la página#}
        <h2 class=\"mt-4 mb-3\">Listado de Cafés</h2>
        {#Para aislarlo del título se envuelve el formulario en un class row#}
        <div class=\"row\">
            <div class=\"col-md-2\">
            </div>
            <div class=\"col-md-8\">
                <table class=\"table\">
                    <thead class=\"thead-dark\">
                    <tr style=\"text-align: center\">
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">Foto</th>
                        <th scope=\"col\">Nombre</th>
                        <th scope=\"col\">Editar</th>
                        <th scope=\"col\">Borrar</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% set contador=0 %}
                    {% for cafe in cafes %}
                        <tr style=\"text-align: center\">
                            {% set contador=contador+1 %}
                            <th scope=\"row\">{{ contador }}</th>
                            {#El campo de la fecha lo filtraremos con date para que muestre el formato deseado#}
                            <td>
                                <img src={% if  cafe.foto|length>0 %}{{ asset(fotosCafes~cafe.foto) }}{% else %}\"http://placehold.it/700x400\"{% endif %}
                                     width=\"200\" alt=\"\">
                            </td>
                            <td>{{ cafe.nombre }}</td>
                            {#{{ dump(categoria.id) }}#}
                            {#para que redireccione con el id la variable enviada se tiene que llamar id, no vale otro nombre#}
                            <td><a href=\"{{ path('nuevoCafe',{'id': cafe.id }) }}\">
                                    <i class=\"fas fa-edit\"
                                       style=\"font-size: 130%\"></i>
                                </a>
                            </td>
                            {#Al asignarle al onclick el return se ejecuta la función y devuelve lo que devuelva esta #}
                            <td><a href=\"#\"
                                   onClick=\"return controlBorrado('{{ path('borrarCafe',{'id': cafe.id }) }}')\">
                                    <i class=\"far fa-trash-alt\"
                                       style=\"font-size: 140%\"></i>
                                </a>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
            <div class=\"col-md-2\">
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    <script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>
    <script src=\"{{ asset('js/controlBorrado.js') }}\"></script>
{% endblock %}

", "gestionCafes/edicionCafes.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\gestionCafes\\edicionCafes.html.twig");
    }
}
