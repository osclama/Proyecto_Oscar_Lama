<?php

/* frontal/listadoCompletoCafes.html.twig */
class __TwigTemplate_99c3540a6cacc979ddd11caa97e19d5b127cf65571b16a927f2ff4bbd00b3897 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/listadoCompletoCafes.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo " Cafés Mauro ";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "    <header>

    </header>
    <div class=\"container\">
        <!-- Portfolio Section -->
        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Todos Nuestros Cafés</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias");
        echo "\">Categorías</a>
            </li>
            <li class=\"breadcrumb-item active\">Cafés</li>
        </ol>
        ";
        // line 27
        echo "        <div class=\"row\">
            ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["datosCafes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cafe"]) {
            // line 29
            echo "                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        ";
            // line 33
            echo "                        ";
            // line 35
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\"><img class=\"card-img-top\"
                                                                          src=";
            // line 36
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((($context["fotosCafes"] ?? null) . twig_get_attribute($this->env, $this->source, $context["cafe"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 37
            echo "                                                                          alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                ";
            // line 41
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cafes", array("id" => twig_get_attribute($this->env, $this->source, $context["cafe"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cafe"], "nombre", array()), "html", null, true);
            echo "</a>
                            </h4>
                            <p class=\"card-text\">";
            // line 43
            echo twig_get_attribute($this->env, $this->source, $context["cafe"], "caracteristicas", array());
            echo "</p>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cafe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "        </div>
        <!-- /.container -->

        <!-- Paginacion -->
        ";
        // line 53
        echo "        ";
        // line 54
        echo "        ";
        if ((($context["numTotalPaginas"] ?? null) > 1)) {
            // line 55
            echo "            <ul class=\"pagination justify-content-center\">
                ";
            // line 57
            echo "                ";
            if ((($context["paginaActual"] ?? null) > 1)) {
                // line 58
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\" href=\"";
                // line 59
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("todosNuestrosCafes", array("pagina" => ((((($context["paginaActual"] ?? null) - 1) < 1)) ? (1) : ((($context["paginaActual"] ?? null) - 1))))), "html", null, true);
                echo "\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                ";
            }
            // line 66
            echo "                ";
            // line 67
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($context["numTotalPaginas"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["indice"]) {
                // line 68
                echo "                    ";
                if (($context["indice"] == ($context["paginaActual"] ?? null))) {
                    // line 69
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("todosNuestrosCafes", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                } else {
                    // line 74
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"";
                    // line 75
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("todosNuestrosCafes", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                }
                // line 78
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                ";
            // line 80
            echo "                ";
            if ((($context["paginaActual"] ?? null) < ($context["numTotalPaginas"] ?? null))) {
                // line 81
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 83
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("todosNuestrosCafes", array("pagina" => ((((($context["paginaActual"] ?? null) + 1) <= ($context["numTotalPaginas"] ?? null))) ? ((($context["paginaActual"] ?? null) + 1)) : (($context["paginaActual"] ?? null))))), "html", null, true);
                echo "\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                ";
            }
            // line 90
            echo "            </ul>
        ";
        }
        // line 92
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "frontal/listadoCompletoCafes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 92,  205 => 90,  195 => 83,  191 => 81,  188 => 80,  186 => 79,  180 => 78,  172 => 75,  169 => 74,  161 => 71,  157 => 69,  154 => 68,  149 => 67,  147 => 66,  137 => 59,  134 => 58,  131 => 57,  128 => 55,  125 => 54,  123 => 53,  117 => 48,  106 => 43,  98 => 41,  93 => 37,  87 => 36,  82 => 35,  80 => 33,  76 => 29,  72 => 28,  69 => 27,  62 => 22,  56 => 19,  42 => 7,  39 => 6,  33 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/listadoCompletoCafes.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\listadoCompletoCafes.html.twig");
    }
}
