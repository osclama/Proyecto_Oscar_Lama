<?php

/* frontal/login.html.twig */
class __TwigTemplate_2496be618f8fe6f9f207ddcbb5e4c3de25a9fa9013bb7693e264d0ffd892f407 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/login.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Login Usuario ";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "    ";
        // line 8
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Login de usuario</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Login</li>
        </ol>
        <!-- Intro Content -->

        <div class=\"row\">
            <div class=\"col-lg-6\">
                ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", array()), "flashbag", array()), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
            // line 27
            echo "                    <p class=\"info\">";
            echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
            echo "</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                ";
        // line 30
        echo "                ";
        if (($context["error"] ?? null)) {
            // line 31
            echo "                    <h3>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", array()), twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", array()), "security"), "html", null, true);
            echo "</h3>
                ";
        }
        // line 33
        echo "                <form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logado");
        echo "\" method=\"post\">
                    ";
        // line 35
        echo "                    <div class=\"control-group form-group\">
                        <div class=\"controls\">
                            <label for=\"username\">Usuario:</label>
                            ";
        // line 39
        echo "                            <input type=\"text\" class=\"form-control\" id=\"username\" name=\"_username\" placeholder=\"email\"
                                   value=\"";
        // line 40
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\"/>
                        </div>
                    </div>
                    <div class=\"control-group form-group\">
                        <div class=\"controls\">
                            <label for=\"password\">Password:</label>
                            <input type=\"password\" class=\"form-control\" placeholder=\"password\" id=\"password\" name=\"_password\"/>
                        </div>
                    </div>
                    <button type=\"submit\" class=\"btn btn-primary\">Login</button>
                </form>
            </div>
            <div class=\"col-lg-6\">
                <h3>¿Cómo registrarse?</h3>
                <p>Con el registro se podrán hacer reservas para nuestros cursos de café de una forma rápida y
                    sencilla</p>
                <p>Mediante la introducción del correo electrónico y la contraseña se podrá realizar una nueva reserva o
                    anulación de esta</p>
                <p>Si aún no te has registrado en la aplicación, haz click en el enlace de <a
                            href=\"";
        // line 59
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("registro");
        echo "\"><strong>REGISTRO</strong></a></p>
            </div>
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "frontal/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 59,  104 => 40,  101 => 39,  96 => 35,  91 => 33,  85 => 31,  82 => 30,  80 => 29,  71 => 27,  67 => 26,  56 => 18,  44 => 8,  42 => 7,  39 => 6,  33 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/login.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\login.html.twig");
    }
}
