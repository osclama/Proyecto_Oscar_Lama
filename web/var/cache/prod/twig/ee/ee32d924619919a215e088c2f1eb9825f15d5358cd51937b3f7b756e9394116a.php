<?php

/* gestionReservas/reservas.html.twig */
class __TwigTemplate_020a54941c89769f47f4d082adf3df16ac624588d7a9b650b2c882ac02962de6 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "gestionReservas/reservas.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_titulo($context, array $blocks = array())
    {
        echo "Listado de Reservas ";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        // line 9
        echo "    <div class=\"container\">
        ";
        // line 11
        echo "        <h1 class=\"mt-4 mb-3\">Listado de Reservas</h1>
        ";
        // line 13
        echo "        <div class=\"row\">
            <div class=\"col-md-2\">
            </div>
            <div class=\"col-md-8\">
                <table class=\"table\">
                    <thead class=\"thead-dark\">
                    <tr style=\"text-align: center\">
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">Fecha</th>
                        <th scope=\"col\">Num Asistentes</th>
                        <th scope=\"col\">Observaciones</th>
                        <th scope=\"col\">Editar</th>
                        <th scope=\"col\">Borrar</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 29
        $context["contador"] = 0;
        // line 30
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["reservas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["reserva"]) {
            // line 31
            echo "                        <tr style=\"text-align: center\">
                            ";
            // line 32
            $context["contador"] = (($context["contador"] ?? null) + 1);
            // line 33
            echo "                            <th scope=\"row\">";
            echo twig_escape_filter($this->env, ($context["contador"] ?? null), "html", null, true);
            echo "</th>
                            ";
            // line 35
            echo "                            <td>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reserva"], "fecha", array()), "Y/m/d h:i"), "html", null, true);
            echo "</td>
                            <td>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reserva"], "asistentes", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 37
            echo twig_get_attribute($this->env, $this->source, $context["reserva"], "observaciones", array());
            echo "</td>
                            ";
            // line 39
            echo "                            <td><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevaReserva", array("id" => twig_get_attribute($this->env, $this->source, $context["reserva"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fas fa-edit\"
                                                                                            style=\"font-size: 130%\"></i></a>
                            </td>
                            ";
            // line 43
            echo "                            <td><a href=\"#\" onClick=\"return controlBorrado('";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("borrarReserva", array("id" => twig_get_attribute($this->env, $this->source, $context["reserva"], "id", array()))), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reserva"], "fecha", array()), "Y/m/d h:i"), "html", null, true);
            echo "')\"> <i class=\"far fa-trash-alt\"
                                                                                             style=\"font-size: 140%\"></i></a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reserva'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "                    </tbody>
                </table>
            </div>
            <div class=\"col-md-2\">
            </div>
        </div>
    </div>
";
    }

    // line 56
    public function block_javascripts($context, array $blocks = array())
    {
        // line 57
        echo "<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>
<script src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/controlBorrado.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "gestionReservas/reservas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 58,  134 => 57,  131 => 56,  120 => 48,  106 => 43,  99 => 39,  95 => 37,  91 => 36,  86 => 35,  81 => 33,  79 => 32,  76 => 31,  71 => 30,  69 => 29,  51 => 13,  48 => 11,  45 => 9,  43 => 8,  40 => 7,  34 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "gestionReservas/reservas.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\gestionReservas\\reservas.html.twig");
    }
}
