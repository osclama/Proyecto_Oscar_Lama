<?php

/* gestionCafes/nuevaCategoria.html.twig */
class __TwigTemplate_a27b0a7785474db5391cae0a33264ba2a4ba1be0cd20897328325d3668f2b4d1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "gestionCafes/nuevaCategoria.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["formularioCategoria"] ?? null), array(0 => "form/cafeForm.html.twig"), true);
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_titulo($context, array $blocks = array())
    {
        echo "Nueva Categoría ";
    }

    // line 8
    public function block_contenido($context, array $blocks = array())
    {
        // line 9
        echo "    ";
        // line 10
        echo "    <div class=\"container\">
        ";
        // line 12
        echo "        <h1 class=\"mt-4 mb-3\">Nueva Categoría</h1>
        ";
        // line 14
        echo "        <div class=\"row\">
            <div class=\"col-md-8\">
                ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioCategoria"] ?? null), 'form_start');
        echo "
                ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["formularioCategoria"] ?? null), 'widget');
        echo "
                ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioCategoria"] ?? null), 'form_end');
        echo "
             </div>
        </div>
    </div>
";
    }

    // line 23
    public function block_javascripts($context, array $blocks = array())
    {
        // line 24
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/nuevaImgCafe.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "gestionCafes/nuevaCategoria.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 24,  75 => 23,  66 => 18,  62 => 17,  58 => 16,  54 => 14,  51 => 12,  48 => 10,  46 => 9,  43 => 8,  37 => 5,  33 => 2,  31 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "gestionCafes/nuevaCategoria.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\gestionCafes\\nuevaCategoria.html.twig");
    }
}
