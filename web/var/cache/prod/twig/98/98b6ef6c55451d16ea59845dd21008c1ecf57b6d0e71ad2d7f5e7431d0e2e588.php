<?php

/* form/cafeForm.html.twig */
class __TwigTemplate_be697eade3d795a0f69c271de0fe34093883a25e8bfd30ee177e04dcee5b8426 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'form_row' => array($this, 'block_form_row'),
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'button_widget' => array($this, 'block_button_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->displayBlock('form_row', $context, $blocks);
        // line 14
        echo "
    ";
        // line 16
        echo "        ";
        // line 17
        echo "        ";
        // line 18
        echo "    ";
        // line 19
        echo "    ";
        // line 20
        echo "        ";
        // line 21
        echo "        ";
        // line 22
        echo "        ";
        // line 23
        echo "        ";
        // line 24
        echo "    ";
        // line 25
        echo "
";
        // line 28
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 39
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 44
        $this->displayBlock('button_widget', $context, $blocks);
    }

    // line 2
    public function block_form_row($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"control-group form-group\">
        <div class=\"controls\">";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        // line 9
        echo "</div>
    </div>";
    }

    // line 28
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 29
        $context["type"] = (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "text")) : ("text"));
        // line 31
        echo "    <input type=\"";
        echo twig_escape_filter($this->env, ($context["type"] ?? null), "html", null, true);
        echo "\"
           class=\"form-control\" ";
        // line 32
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty(($context["value"] ?? null))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "\" ";
        }
        echo "/>
    ";
        // line 33
        if ((($context["type"] ?? null) == "file")) {
            // line 34
            echo "        <img id=\"cafeThumb\" class=\"img-fluid\" width=\"150px\" src=\"\" alt=\"\">
    ";
        }
    }

    // line 39
    public function block_textarea_widget($context, array $blocks = array())
    {
        // line 40
        echo "<textarea class=\"form-control\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
        echo "</textarea>";
    }

    // line 44
    public function block_button_widget($context, array $blocks = array())
    {
        // line 45
        if (twig_test_empty(($context["label"] ?? null))) {
            // line 46
            if ( !twig_test_empty(($context["label_format"] ?? null))) {
                // line 47
                $context["label"] = twig_replace_filter(($context["label_format"] ?? null), array("%name%" =>                 // line 48
($context["name"] ?? null), "%id%" =>                 // line 49
($context["id"] ?? null)));
            } elseif ((            // line 51
($context["label"] ?? null) === false)) {
                // line 52
                $context["translation_domain"] = false;
            } else {
                // line 54
                $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
            }
        }
        // line 58
        echo "    <button type=\"";
        echo twig_escape_filter($this->env, (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "button")) : ("button")), "html", null, true);
        echo "\"
            class=\"btn btn-primary\" ";
        // line 59
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? null) === false)) ? (($context["label"] ?? null)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), array(), ($context["translation_domain"] ?? null)))), "html", null, true);
        echo "</button>";
    }

    public function getTemplateName()
    {
        return "form/cafeForm.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  142 => 59,  137 => 58,  133 => 54,  130 => 52,  128 => 51,  126 => 49,  125 => 48,  124 => 47,  122 => 46,  120 => 45,  117 => 44,  109 => 40,  106 => 39,  100 => 34,  98 => 33,  88 => 32,  83 => 31,  81 => 29,  78 => 28,  73 => 9,  71 => 8,  69 => 7,  67 => 6,  64 => 4,  61 => 2,  57 => 44,  55 => 39,  53 => 28,  50 => 25,  48 => 24,  46 => 23,  44 => 22,  42 => 21,  40 => 20,  38 => 19,  36 => 18,  34 => 17,  32 => 16,  29 => 14,  27 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "form/cafeForm.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\form\\cafeForm.html.twig");
    }
}
