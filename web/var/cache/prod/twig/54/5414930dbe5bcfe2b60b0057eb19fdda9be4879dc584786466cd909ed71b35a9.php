<?php

/* gestionReservas/nuevaReserva.html.twig */
class __TwigTemplate_a98bd26a66aae6e0df421f9897f84bf5ef652eeeaef09cef096a69fdaf778acb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "gestionReservas/nuevaReserva.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["formularioReserva"] ?? null), array(0 => "form/cafeForm.html.twig"), true);
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_titulo($context, array $blocks = array())
    {
        echo "Nueva Reserva ";
    }

    // line 8
    public function block_contenido($context, array $blocks = array())
    {
        // line 9
        echo "    ";
        // line 10
        echo "    <div class=\"container\">
        ";
        // line 12
        echo "        <h1 class=\"mt-4 mb-3\">Nueva Reserva</h1>
        ";
        // line 14
        echo "        <div class=\"row\">
            <div class=\"col-md-8\">
                ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioReserva"] ?? null), 'form_start');
        echo "
                ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["formularioReserva"] ?? null), 'widget');
        echo "
                ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioReserva"] ?? null), 'form_end');
        echo "
             </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "gestionReservas/nuevaReserva.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 18,  61 => 17,  57 => 16,  53 => 14,  50 => 12,  47 => 10,  45 => 9,  42 => 8,  36 => 5,  32 => 2,  30 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "gestionReservas/nuevaReserva.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\gestionReservas\\nuevaReserva.html.twig");
    }
}
