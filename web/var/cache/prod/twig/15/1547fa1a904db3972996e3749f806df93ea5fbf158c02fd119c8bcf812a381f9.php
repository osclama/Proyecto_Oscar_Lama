<?php

/* frontal/listadoCategorias.html.twig */
class __TwigTemplate_dd2f41e1a5ea911d35e751170413a3c7aa36c24604f2e9cee3065689e8a68b80 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/listadoCategorias.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo " Cafés Mauro ";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "    <header>

    </header>
    <div class=\"container\">
        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Categorías</small>
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Categorías</li>
        </ol>
        <!-- Image Header -->
        <!-- Portfolio Section -->
        ";
        // line 25
        echo "        <div class=\"row\">
            ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["datosCategorias"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            // line 27
            echo "                <div class=\"col-lg-4 col-sm-6 portfolio-item\">
                    <div class=\"card h-100\">
                        ";
            // line 31
            echo "                        ";
            // line 33
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("categoria", array("id" => twig_get_attribute($this->env, $this->source, $context["categoria"], "id", array()))), "html", null, true);
            echo "\"><img class=\"card-img-top\"
                                                                                   src=";
            // line 34
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categoria"], "foto", array())) > 0)) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((($context["fotosCategorias"] ?? null) . twig_get_attribute($this->env, $this->source, $context["categoria"], "foto", array()))), "html", null, true);
            } else {
                echo "\"http://placehold.it/700x400\"";
            }
            // line 35
            echo "                                                                                   alt=\"\"></a>
                        <div class=\"card-body\">
                            <h4 class=\"card-title\">
                                ";
            // line 39
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("categoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "id", array()), "nombreCategoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()), "html", null, true);
            echo "</a>
                            </h4>
                            <p class=\"card-text\">";
            // line 41
            echo twig_get_attribute($this->env, $this->source, $context["categoria"], "descripcion", array());
            echo "</p>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </div>
        <!-- /.container -->
        ";
        // line 49
        echo "
        <!-- Paginacion -->
        ";
        // line 52
        echo "        ";
        if ((($context["numTotalPaginas"] ?? null) > 1)) {
            // line 53
            echo "            <ul class=\"pagination justify-content-center\">
                ";
            // line 55
            echo "                ";
            if ((($context["paginaActual"] ?? null) > 1)) {
                // line 56
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 58
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => ((((($context["paginaActual"] ?? null) - 1) < 1)) ? (1) : ((($context["paginaActual"] ?? null) - 1))))), "html", null, true);
                echo "\"
                           aria-label=\"Anterior\">
                            <span aria-hidden=\"true\">&laquo;</span>
                            <span class=\"sr-only\">Anterior</span>
                        </a>
                    </li>
                ";
            }
            // line 65
            echo "                ";
            // line 66
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($context["numTotalPaginas"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["indice"]) {
                // line 67
                echo "
                    ";
                // line 68
                if (($context["indice"] == ($context["paginaActual"] ?? null))) {
                    // line 69
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link font-weight-bold\"
                               href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                } else {
                    // line 74
                    echo "                        <li class=\"page-item\">
                            <a class=\"page-link\"
                               href=\"";
                    // line 76
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => $context["indice"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["indice"], "html", null, true);
                    echo "</a>
                        </li>
                    ";
                }
                // line 79
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 80
            echo "                ";
            // line 81
            echo "                ";
            if ((($context["paginaActual"] ?? null) < ($context["numTotalPaginas"] ?? null))) {
                // line 82
                echo "                    <li class=\"page-item\">
                        <a class=\"page-link\"
                           href=\"";
                // line 84
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias", array("pagina" => ((((($context["paginaActual"] ?? null) + 1) <= ($context["numTotalPaginas"] ?? null))) ? ((($context["paginaActual"] ?? null) + 1)) : (($context["paginaActual"] ?? null))))), "html", null, true);
                echo "\"
                           aria-label=\"Siguiente\">
                            <span aria-hidden=\"true\">&raquo;</span>
                            <span class=\"sr-only\">Siguiente</span>
                        </a>
                    </li>
                ";
            }
            // line 91
            echo "            </ul>
        ";
        }
        // line 93
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "frontal/listadoCategorias.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 93,  204 => 91,  194 => 84,  190 => 82,  187 => 81,  185 => 80,  179 => 79,  171 => 76,  167 => 74,  159 => 71,  155 => 69,  153 => 68,  150 => 67,  145 => 66,  143 => 65,  133 => 58,  129 => 56,  126 => 55,  123 => 53,  120 => 52,  116 => 49,  112 => 46,  101 => 41,  93 => 39,  88 => 35,  82 => 34,  77 => 33,  75 => 31,  71 => 27,  67 => 26,  64 => 25,  55 => 18,  42 => 7,  39 => 6,  33 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/listadoCategorias.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\listadoCategorias.html.twig");
    }
}
