<?php

/* frontal/cafes.html.twig */
class __TwigTemplate_26c4d8534ffa34964dd31718cd0f7666bf0fd2bdbc534e582a18b18eb2f3bfdc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/cafes.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Cafés ";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Datos del Café
        </h1>

        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCategorias");
        echo "\">Categorías</a>
            </li>
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("todosNuestrosCafes");
        echo "\">Cafes</a>
            </li>
            <li class=\"breadcrumb-item active\">Datos del Café</li>
        </ol>

        <!-- Portfolio Item Row -->
        <div class=\"row\">

            <div class=\"col-md-12 text-center\">
                ";
        // line 33
        echo "                <img class=\"img-fluid\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((($context["fotosCafes"] ?? null) . twig_get_attribute($this->env, $this->source, ($context["datosCafe"] ?? null), "foto", array()))), "html", null, true);
        echo "\" alt=\"\">
            </div>

            <div class=\"col-md-12\">
                <h2 class=\"my-4 text-primary\">";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datosCafe"] ?? null), "nombre", array()), "html", null, true);
        echo "</h2>


                <div>
                        <h4 class=\"my-3\">Categorías del café                        </h4>

                    ";
        // line 44
        echo "                            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["datosCafe"] ?? null), "categorias", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            // line 45
            echo "                                ";
            // line 46
            echo "
                                <a class=\"btn btn-primary m-1\"
                                   href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listadoCafes", array("categoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "id", array()), "nombreCategoria" => twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categoria"], "nombre", array()), "html", null, true);
            echo "</a>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                </div>

                <h4 class=\"my-3\">Descripción</h4>

                <p>";
        // line 54
        echo twig_get_attribute($this->env, $this->source, ($context["datosCafe"] ?? null), "descripcion", array());
        echo "</p>
                <h4 class=\"my-3\">Características</h4>
                <p>";
        // line 56
        echo twig_get_attribute($this->env, $this->source, ($context["datosCafe"] ?? null), "caracteristicas", array());
        echo "</p>
            </div>
            <div class=\"float-none\"></div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "frontal/cafes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 56,  121 => 54,  115 => 50,  105 => 48,  101 => 46,  99 => 45,  94 => 44,  85 => 37,  77 => 33,  65 => 22,  59 => 19,  53 => 16,  42 => 7,  39 => 6,  33 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/cafes.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\cafes.html.twig");
    }
}
