<?php

/* gestionCafes/edicionDatosUsuarios.html.twig */
class __TwigTemplate_6554f32845a65460c566fb0c35442e60de2c407bf9f857cb31e32dfa1ac14c55 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "gestionCafes/edicionDatosUsuarios.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["formularioUsuario"] ?? null), array(0 => "form/cafeForm.html.twig"), true);
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_titulo($context, array $blocks = array())
    {
        echo "Registro Usuario ";
    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        // line 11
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\"> Cafés Mauro
            <small>Edición de Usuario</small>
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"breadcrumb-item\">
                <a href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Inicio</a>
            </li>
            <li class=\"breadcrumb-item active\">Edicion Datos Usuarios</li>
        </ol>
        <!-- Intro Content -->
        <div class=\"row\">
            <div class=\"col-lg-6\">
                ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioUsuario"] ?? null), 'form_start');
        echo "
                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["formularioUsuario"] ?? null), 'widget');
        echo "
                ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["formularioUsuario"] ?? null), 'form_end');
        echo "
            </div>
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "gestionCafes/edicionDatosUsuarios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 29,  72 => 28,  68 => 27,  58 => 20,  47 => 11,  45 => 10,  42 => 9,  36 => 6,  32 => 2,  30 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "gestionCafes/edicionDatosUsuarios.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\gestionCafes\\edicionDatosUsuarios.html.twig");
    }
}
