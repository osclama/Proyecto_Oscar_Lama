<?php

/* frontal/locales.html.twig */
class __TwigTemplate_07a5140c71f216c0272ae32e9ff83978a26d0b73beb1f3ad5393294c336a2c32 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "frontal/locales.html.twig", 2);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Locales ";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "    <!-- Page Content -->
    <div class=\"container\">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class=\"mt-4 mb-3\">Cafés Mauro
            <small>Locales</small>
        </h1>
        ";
        // line 15
        echo "        ";
        // line 16
        echo "        ";
        if (((($context["sitio"] ?? null) == "central") || (($context["sitio"] ?? null) == "todos"))) {
            // line 17
            echo "            <!-- Local Agrela -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3073.44401512575!2d-8.4219075788343!3d43.35726570275931!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2e7cf29c5a1801%3A0x187698011fa57edf!2sCalle+Pasteur%2C+14%2C+15008+La+Coru%C3%B1a!5e0!3m2!1ses!2ses!4v1541544687410\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Cafés Mauro
                        <br>Pol.Agrela, Pasteur, 14-15008 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 981274012
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        ";
        }
        // line 48
        echo "
        ";
        // line 50
        echo "        ";
        if (((($context["sitio"] ?? null) == "ruaNova") || (($context["sitio"] ?? null) == "todos"))) {
            // line 51
            echo "            <!-- Local Rua Nova -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4632.25942513763!2d-8.40440362285743!3d43.37081748708746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7a35331274711cf0!2sCaf%C3%A9+Siboney!5e1!3m2!1ses!2ses!4v1544095477090\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Café & Boutique Mauro
                        <br> Rua Nova, 28 - 15003 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 981 227 836
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        ";
        }
        // line 82
        echo "
        ";
        // line 84
        echo "        ";
        if (((($context["sitio"] ?? null) == "ruaFerrol") || (($context["sitio"] ?? null) == "todos"))) {
            // line 85
            echo "            <!-- Local Rúa Ferrol -->
            <div class=\"row\">
                <!-- Map Column -->
                <div class=\"col-lg-8 mb-4\">
                    <!-- Embedded Google Map -->
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2900.5814027398446!2d-8.410197908706213!3d43.364865892505705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xebf814e6bdd12fc0!2sCaf%C3%A9+Siboney!5e0!3m2!1ses!2ses!4v1544095393695\"
                            width=\"700\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>

                </div>
                <!-- Contact Details Column -->
                <div class=\"col-lg-4 mb-4\">
                    <h3>Localización</h3>
                    <p>
                        Café & Boutique Mauro
                        <br> Rua Ferrol, 14 - 15004 (A Coruña)
                        <br>
                    </p>
                    <p>
                        <abbr title=\"Phone\">t</abbr>: 881 899 142
                    </p>
                    <p>
                        <abbr title=\"Email\">Email</abbr>:
                        <a href=\"mailto:name@example.com\">cafesMauro@example.com
                        </a>
                    </p>
                    <p>
                        <abbr title=\"Hours\">Horario</abbr>: Lunes - Viernes: 9:00 AM - 19:00 PM
                    </p>
                </div>
            </div>
            <!-- /.row -->
        ";
        }
        // line 117
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "frontal/locales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 117,  134 => 85,  131 => 84,  128 => 82,  95 => 51,  92 => 50,  89 => 48,  56 => 17,  53 => 16,  51 => 15,  42 => 7,  39 => 6,  33 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "frontal/locales.html.twig", "C:\\xampp\\htdocs\\web\\FCT\\REPOSITORIO_GITLAB_FCT\\proyecto_fct\\web\\app\\Resources\\views\\frontal\\locales.html.twig");
    }
}
