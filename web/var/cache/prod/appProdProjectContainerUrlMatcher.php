<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // app_api_listacategorias
        if ('/api/listarCategorias' === $pathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\ApiController::listaCategoriasAction',  '_route' => 'app_api_listacategorias',);
            if (!in_array($canonicalMethod, array('GET'))) {
                $allow = array_merge($allow, array('GET'));
                goto not_app_api_listacategorias;
            }

            return $ret;
        }
        not_app_api_listacategorias:

        // app_api_insertarcategoria
        if (0 === strpos($pathinfo, '/api/insertarCategoria') && preg_match('#^/api/insertarCategoria(?:/(?P<nombre>[^/]++)(?:/(?P<descripcion>[^/]++))?)?$#sD', $pathinfo, $matches)) {
            $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'app_api_insertarcategoria')), array (  'nombre' => '',  'descripcion' => '',  '_controller' => 'AppBundle\\Controller\\ApiController::insertarCategoriaAction',));
            if (!in_array($requestMethod, array('POST'))) {
                $allow = array_merge($allow, array('POST'));
                goto not_app_api_insertarcategoria;
            }

            return $ret;
        }
        not_app_api_insertarcategoria:

        // homepage
        if (preg_match('#^/(?P<pagina>\\d+)?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'homepage')), array (  'pagina' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::homeAction',));
        }

        // nosotros
        if (0 === strpos($pathinfo, '/nosotros') && preg_match('#^/nosotros(?:/(?P<ruta>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'nosotros')), array (  'ruta' => NULL,  '_controller' => 'AppBundle\\Controller\\DefaultController::nosotrosAction',));
        }

        // contactar
        if (0 === strpos($pathinfo, '/contactar') && preg_match('#^/contactar(?:/(?P<sitio>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'contactar')), array (  'sitio' => 'todos',  '_controller' => 'AppBundle\\Controller\\DefaultController::contactarAction',));
        }

        // cafes
        if (0 === strpos($pathinfo, '/cafes') && preg_match('#^/cafes(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cafes')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\DefaultController::cafeAction',));
        }

        // todosNuestrosCafes
        if (0 === strpos($pathinfo, '/todosNuestrosCafes') && preg_match('#^/todosNuestrosCafes(?:/(?P<pagina>\\d+))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'todosNuestrosCafes')), array (  'pagina' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::todosNuestrosCafesAction',));
        }

        // listadoCategorias
        if (0 === strpos($pathinfo, '/listadoCategorias') && preg_match('#^/listadoCategorias(?:/(?P<pagina>\\d+))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'listadoCategorias')), array (  'pagina' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::listadoCategoriasAction',));
        }

        // listadoCafes
        if (preg_match('#^/(?P<categoria>[^/]++)/listadoCafes(?:/(?P<pagina>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'listadoCafes')), array (  'pagina' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::listadoCafesAction',));
        }

        // categoria
        if (0 === strpos($pathinfo, '/categoria') && preg_match('#^/categoria(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'categoria')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\DefaultController::categoriaAction',));
        }

        // registro
        if ('/registro' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::registroAction',  '_route' => 'registro',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_registro;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'registro'));
            }

            return $ret;
        }
        not_registro:

        if (0 === strpos($pathinfo, '/reservas')) {
            // nuevaReserva
            if (0 === strpos($pathinfo, '/reservas/nueva') && preg_match('#^/reservas/nueva(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'nuevaReserva')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionReservasController::nuevaReservaAction',));
            }

            // reservas
            if ('/reservas/reservas' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GestionReservasController::reservasAction',  '_route' => 'reservas',);
            }

            // borrarReserva
            if (0 === strpos($pathinfo, '/reservas/borrar') && preg_match('#^/reservas/borrar(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'borrarReserva')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionReservasController::borrarReservaAction',));
            }

        }

        // logado
        if ('/login' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::loginAction',  '_route' => 'logado',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_logado;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'logado'));
            }

            return $ret;
        }
        not_logado:

        // logout
        if ('/logout' === $pathinfo) {
            return array('_route' => 'logout');
        }

        if (0 === strpos($pathinfo, '/gestionCafes')) {
            // nuevoCafe
            if (0 === strpos($pathinfo, '/gestionCafes/nuevoCafe') && preg_match('#^/gestionCafes/nuevoCafe(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'nuevoCafe')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionCafesController::nuevoCafeAction',));
            }

            // nuevaCategoria
            if (0 === strpos($pathinfo, '/gestionCafes/nuevaCategoria') && preg_match('#^/gestionCafes/nuevaCategoria(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'nuevaCategoria')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionCafesController::nuevaCategoriaAction',));
            }

            // edicionCafes
            if ('/gestionCafes/edicionCafes' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GestionCafesController::edicionCafesAction',  '_route' => 'edicionCafes',);
            }

            // edicionCategorias
            if ('/gestionCafes/edicionCategorias' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GestionCafesController::edicionCategoriasAction',  '_route' => 'edicionCategorias',);
            }

            // borrarCafe
            if (0 === strpos($pathinfo, '/gestionCafes/borrarCafe') && preg_match('#^/gestionCafes/borrarCafe(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'borrarCafe')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionCafesController::borrarCafeAction',));
            }

            // borrarCategoria
            if (0 === strpos($pathinfo, '/gestionCafes/borrarCategoria') && preg_match('#^/gestionCafes/borrarCategoria(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'borrarCategoria')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionCafesController::borrarCategoriaAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/usuarios/edicion')) {
            // edicionReservas
            if ('/usuarios/edicionReservas' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GestionUsuariosController::edicionReservasAction',  '_route' => 'edicionReservas',);
            }

            // edicionUsuarios
            if ('/usuarios/edicionUsuarios' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GestionUsuariosController::edicionUsuariosAction',  '_route' => 'edicionUsuarios',);
            }

            // edicionDatosUsuarios
            if (0 === strpos($pathinfo, '/usuarios/edicionDatosUsuarios') && preg_match('#^/usuarios/edicionDatosUsuarios(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edicionDatosUsuarios')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionUsuariosController::edicionDatosUsuariosAction',));
            }

        }

        // borrarUsuario
        if (0 === strpos($pathinfo, '/usuarios/borrarUsuario') && preg_match('#^/usuarios/borrarUsuario(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'borrarUsuario')), array (  'id' => NULL,  '_controller' => 'AppBundle\\Controller\\GestionUsuariosController::borrarUsuarioAction',));
        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
